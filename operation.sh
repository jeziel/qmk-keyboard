COMMAND=$1

USER=$(whoami)
QMK_HOME=/qmk_firmware
QMK_KEYBOARDS=$QMK_HOME/keyboards
BASE_IMG_NAME=qmk-${USER}-base:latest
KEYB_IMG_NAME=qmk-${USER}-keyboards:latest

case $COMMAND in
    login)
        docker login registry.gitlab.com -u $USER -p $GL_CONT_AK
        ;;
    pull)
        docker pull registry.gitlab.com/jeziel/qmk-keyboard:master
        ;;
    build-base)
        echo "Building base locally."
        docker build -f Dockerfile.base -t $BASE_IMG_NAME --no-cache .
        ;;
    build)
        echo "Building image for user: '$USER', in QMK_HOME: '$QMK_HOME'."
        docker build \
            --build-arg ARG_USER=$USER \
            --build-arg ARG_QMK_HOME=$QMK_HOME \
            -f Dockerfile --tag $KEYB_IMG_NAME --no-cache .
        ;;
    debug)
        docker run --rm -it \
            $KEYB_IMG_NAME bash
        ;;
    compile)
        PWD=$(pwd)
        KEYBOARD=$2
        docker run --rm \
            -v $PWD/src/keyboard/planck:${QMK_KEYBOARDS}/planck/keymaps/${USER} \
            -v $PWD/src/keyboard/gergoplex:${QMK_KEYBOARDS}/gergoplex/keymaps/${USER} \
            -v $PWD/src/keyboard/bdn9:${QMK_KEYBOARDS}/keebio/bdn9/keymaps/${USER} \
            -v $PWD/src/keyboard/bfo9000:${QMK_KEYBOARDS}/keebio/bfo9000/keymaps/${USER} \
            -v $PWD/src/keyboard/kyria:${QMK_KEYBOARDS}/splitkb/kyria/keymaps/${USER} \
            -v $PWD/src/keyboard/romac_plus:${QMK_KEYBOARDS}/kingly_keys/romac_plus/keymaps/${USER} \
            -v $PWD/src/keyboard/naked48:${QMK_KEYBOARDS}/salicylic_acid3/naked48/rev1/keymaps/${USER} \
            -v $PWD/src/keyboard/microdox:${QMK_KEYBOARDS}/boardsource/microdox/v1/keymaps/${USER} \
            -v $PWD/src/keyboard/technik:${QMK_KEYBOARDS}/boardsource/technik_o/keymaps/${USER} \
            -v $PWD/src/keyboard/rhymestone:${QMK_KEYBOARDS}/marksard/rhymestone/keymaps/${USER} \
            -v $PWD/src/keyboard/pancakev2:${QMK_KEYBOARDS}/spaceman/pancake/rev2/keymaps/${USER} \
            -v $PWD/src/keyboard/ferris:${QMK_KEYBOARDS}/ferris/keymaps/${USER} \
            -v $PWD/src/keyboard/corne:${QMK_KEYBOARDS}/crkbd/keymaps/${USER} \
            -v $PWD/src/keyboard/commons:${QMK_HOME}/users/${USER} \
            -v $PWD/script:/root/script \
            -v $PWD/target:/root/target \
            $KEYB_IMG_NAME \
            /root/script/compile.sh ${KEYBOARD}
        ;;
    *)
        echo "Usage:"
        echo " login  # logs in the container registry."
        echo " pull   # gets already build image from  container registry."
        echo " build  # build a new qmk-base docker image."
        echo " debug  # allows you to run a container on the keyboard image."
        echo " compile [planck|gergoplex|bdn9|kyria|romac_plus|bfo9000|naked48|microdox|draculad|technik|rhymestone|pancakev2]"
        exit 1
        ;;
esac

#!/usr/bin/env bash

KEYBOARD=$1
# from env: USER=jeziel

compile() {
    KEYMAP=$1
    KBD=$2
    OUT=$3

    cd $QMK_HOME
    echo "Compiling Keyboard:${KBD} with KeyMap:${KEYMAP}"
    qmk compile --clean -kb $KBD -km $KEYMAP && \
        cp $OUT /root/target
}

case $KEYBOARD in
    planck)
        compile $USER planck/ez planck_ez_base_${USER}.bin
        ;;
    gergoplex)
        compile $USER gergoplex gergoplex_${USER}.hex
        ;;
    bdn9)
        compile $USER keebio/bdn9 keebio_bdn9_rev1_${USER}.hex
        ;;
    kyria)
        compile $USER kyria splitkb_kyria_rev1_${USER}.hex
        ;;
    romac_plus)
        compile $USER kingly_keys/romac_plus kingly_keys_romac_plus_${USER}.hex
        ;;
    bfo9000)
        compile $USER keebio/bfo9000 keebio_bfo9000_${USER}.hex
        ;;
    naked48)
        compile $USER naked48 salicylic_acid3_naked48_rev1_${USER}.hex
        ;;
    microdox)
        compile $USER boardsource/microdox/v1 boardsource_microdox_v1_${USER}.hex
        ;;
    technik)
        compile $USER boardsource/technik_o boardsource_technik_o_${USER}.hex
        ;;
    rhymestone)
        compile $USER marksard/rhymestone marksard_rhymestone_rev1_${USER}.hex
        ;;
    pancakev2)
        compile $USER spaceman/pancake/rev2 spaceman_pancake_rev2_${USER}.hex
        ;;
    ferris)
        compile $USER ferris/sweep ferris_sweep_${USER}.uf2
        ;;
    corne)
        compile $USER crkbd/rev1 crkbd_rev1_${USER}.hex
        ;;
    *)
        echo "compile [planck|gergoplex|bdn9|kyria|romac_plus|bfo9000|naked48|microdox|technik|rhymestone|pancakev2|ferris|corne]"
        exit 1
        ;;
esac

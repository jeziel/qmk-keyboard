/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "jeziel.h"

enum layers {
    _MAIN = 0,
    _LOWER,
};

#include "local/main.h"

enum custom_keycodes {
    CT_ENC1 = SAFE_RANGE,
    CT_ENC2,
    CT_VTRE,
	CT_VEZM
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [_MAIN] = LAYOUT(
      XXXXXXX, KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,                                                 KC_F,    KC_G,    KC_C,   KC_R, KC_L, XXXXXXX,
      XXXXXXX, KC_A,    KC_O,    KC_E,    KC_U,    KC_I,                                                 KC_D,    KC_H,    KC_T,   KC_N, KC_S, XXXXXXX,
      XXXXXXX, LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    XXXXXXX, XXXXXXX,  /* | */ XXXXXXX, XXXXXXX, KC_B,    KC_M,    KC_W,   KC_V, LS_Z, XXXXXXX,
                                 CT_ENC1, XXXXXXX, KC_LCMD, LOWER,   XXXXXXX,  /* | */ XXXXXXX, KC_SPC,  KC_RCTL, XXXXXXX, CT_ENC2
    ),

    [_LOWER] = LAYOUT(
    XXXXXXX, DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB,                   /* | */                   DN_VSCH, KC_7,    KC_8,    KC_9, XXXXXXX,   XXXXXXX,
    XXXXXXX, KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,                     /* | */                   DN_VTSH, KC_4,    KC_5,    KC_6, KC_0,      XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX,   KC_PGDOWN, XXXXXXX, XXXXXXX, /* | */ XXXXXXX, XXXXXXX, DN_VMRK, KC_1,    KC_2,    KC_3, KC_KP_DOT, XXXXXXX,
                                _______, _______,   _______,   _______, XXXXXXX, /* | */ XXXXXXX, DN_VFLS, CT_VTRE, CT_VEZM, _______
    ),

};

bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index > SECONDARY_ROTARY) {
        return false;
    }

#ifdef EVENTS_ENABLED
    emit_rotary_event((clockwise) ? ROTARY_CW : ROTARY_CCW, index);
#endif

    return false;
}

void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_key_event(MAIN_SCROLL);
#endif
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (!record->event.pressed) {
        return true;
    }

    switch(keycode) {
#ifdef EVENTS_ENABLED
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_ENC1: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
        case CT_ENC2: emit_rotary_event(ROTARY_CLICK, SECONDARY_ROTARY); break;
#endif
        default: break;
    }

    return true;
};

#ifdef ENCODER_ENABLE

RotaryState primary_rotary_state;
RotaryState secondary_rotary_state;

RotaryState *get_state(Event event) {
    RotaryState *rotary_state;

    switch(event.rotary_position) {
        case PRIMARY_ROTARY: rotary_state = &primary_rotary_state; break;
        default: rotary_state = &secondary_rotary_state;
    }

    return rotary_state;
}

#endif

#pragma once

#define EVENTS_ENABLED

#ifdef ENCODER_ENABLE
  #define ENCODER_RESOLUTION 2
#endif

#ifdef COMBO_ENABLE
  #if defined(COMBO_COUNT)
    #undef COMBO_COUNT
  #endif
  #define COMBO_COUNT 7
  #define CUSTOM_COMBOS
#endif

#ifdef OLED_DRIVER_ENABLE
  #define OLED_FONT_H "keyboards/kingly_keys/romac_plus/keymaps/jeziel/myfont.c"
#endif


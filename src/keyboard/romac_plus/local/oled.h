#ifdef OLED_DRIVER_ENABLE

#include QMK_KEYBOARD_H

static const char PROGMEM nav_logo[4][6] = {
    {0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0},
    {0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0},
    {0xaa, 0xab, 0xac, 0xad, 0xae, 0},
    {0xca, 0xcb, 0xcc, 0xcd, 0xce, 0}
};

static const char PROGMEM number_logo[4][6] = {
    {0x65, 0x66, 0x67, 0x68, 0x69, 0},
    {0x85, 0x86, 0x87, 0x88, 0x89, 0},
    {0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0},
    {0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0}
};

static const char PROGMEM vim_logo[4][6] = {
    {0x60, 0x61, 0x62, 0x63, 0x64, 0},
    {0x80, 0x81, 0x82, 0x83, 0x84, 0},
    {0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0},
    {0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0}
};

void render_logo(const char logo[4][6]) {
    for(int i = 0; i < 4; i ++) {
        //oled_set_cursor(10, i);
        //oled_write_P(nav_logo[i], false);
        oled_write_ln_P(logo[i], false);
    }
}

void oled_render_nav_logo(void) {
    render_logo(nav_logo);
}

void oled_render_number_logo(void) {
    render_logo(number_logo);
}

void oled_render_vim_logo(void) {
    render_logo(vim_logo);
}

void layer_change_oled_handler(Event event) {
    Layers layer = event.layer;
    switch(layer) {
        case NAV: oled_render_nav_logo(); break;
        case VIM: oled_render_vim_logo(); break;
        case NUMBERS: oled_render_number_logo(); break;
    }
}

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    return OLED_ROTATION_0;  // flips the display 180 degrees if offhand
}

#endif

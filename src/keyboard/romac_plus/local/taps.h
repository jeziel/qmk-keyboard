#if defined(TAP_DANCE_ENABLE)

#include QMK_KEYBOARD_H

void dance_layer(qk_tap_dance_state_t *state, void *user_data) {
    switch (state->count) {
        case 2: emit_layer_event(VIM); break;
        case 3: emit_layer_event(NUMBERS); break;
        default: emit_layer_event(NAV); break;
    }

    reset_tap_dance(state);
}

enum dances {
    TD_LAYER_SELECTION,
    TD_VIM_SEARCH,
    TD_VIM_TERM_SEARCH,
    TD_VIM_MARK,
    TD_VIM_FILES,
    TD_GRL_HME_END,
    TD_ITM_NAV,
};

#define DN_LYR  TD(TD_LAYER_SELECTION)
#define DN_VSCH TD(TD_VIM_SEARCH)
#define DN_VTSH TD(TD_VIM_TERM_SEARCH)
#define DN_VMRK TD(TD_VIM_MARK)
#define DN_VFLS TD(TD_VIM_FILES)
#define DN_HMND TD(TD_GRL_HME_END)
#define DN_ITMN TD(TD_ITM_NAV)

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_LAYER_SELECTION] = ACTION_TAP_DANCE_FN(dance_layer),
    [TD_VIM_TERM_SEARCH] = ACTION_DOUBLE_TAP_EVENT(VIM_TERM_SEARCH, VIM_TERM_SEARCH_CLEAR),
    [TD_VIM_SEARCH]      = ACTION_DOUBLE_TAP_EVENT(VIM_SEARCH, VIM_SEARCH_CLEAR),
    [TD_VIM_MARK]        = ACTION_DOUBLE_TAP_EVENT(VIM_MARK_SET, VIM_MARK_GO),
    [TD_VIM_FILES]       = ACTION_DOUBLE_TAP_EVENT(VIM_FILES_OPEN, VIM_FILES_CLOSE),
    [TD_GRL_HME_END]     = ACTION_TAP_DANCE_DOUBLE(KC_HOME, KC_END),
    [TD_ITM_NAV]         = ACTION_DOUBLE_TAP_EVENT(ITM_SCROLL, ITM_FIND),
};

#endif

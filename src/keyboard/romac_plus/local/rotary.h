
//
// STATE
//

typedef struct {
    RotaryEventHandler turn_handler;
    RotaryEventHandler click_handler;
} RotaryEventHandlerState;
RotaryEventHandlerState rotary_handler_state;

typedef struct {
    bool search_rotary_toggle_state;
    bool vim_default_rotary_toggle_state;
    bool nav_rotary_toggle_state;
    bool iterm_nav_rotary_toggle_state;
    bool iterm_find_rotary_toggle_state;
} RotaryToggleState;
RotaryToggleState rotary_toggle_state;

void set_rotary_handlers(RotaryEventHandler turn, RotaryEventHandler click) {
    rotary_handler_state.turn_handler = turn;
    rotary_handler_state.click_handler = click;
    rotary_toggle_state.search_rotary_toggle_state = false;
    rotary_toggle_state.vim_default_rotary_toggle_state = false;
    rotary_toggle_state.nav_rotary_toggle_state = false;
    rotary_toggle_state.iterm_nav_rotary_toggle_state = false;
    rotary_toggle_state.iterm_find_rotary_toggle_state = false;
}

//
// VIM
//

void vim_default_rotary_click_handler(Event event) {
    bool toggle = rotary_toggle_state.vim_default_rotary_toggle_state;

    rotary_handler_state.turn_handler = (toggle)
        ? vim_line_scroll_rotary
        : vim_block_scroll_rotary;

    rotary_toggle_state.vim_default_rotary_toggle_state = !toggle;
}

void vim_search_rotary_click_handler(Event event) {
    bool toggle = rotary_toggle_state.search_rotary_toggle_state;

    rotary_handler_state.turn_handler = (toggle)
        ? vim_search_rotary
        : vim_line_scroll_rotary;

    rotary_toggle_state.search_rotary_toggle_state = !toggle;
}

void nav_scroll_rotary_click_handler(Event event) {
    bool toggle = rotary_toggle_state.nav_rotary_toggle_state;

    rotary_handler_state.turn_handler = (toggle)
        ? main_up_down_rotary
        : main_pgup_down_rotary;

    rotary_toggle_state.nav_rotary_toggle_state = !toggle;
}

//
// ITERM
//

void iterm_nav_rotary_click_handler(Event event) {
    bool toggle = rotary_toggle_state.iterm_nav_rotary_toggle_state;

    rotary_handler_state.turn_handler = (toggle)
        ? iterm_line_scroll_rotary
        : iterm_block_scroll_rotary;

    rotary_toggle_state.iterm_nav_rotary_toggle_state = !toggle;
}

void iterm_find_rotary_click_handler(Event event) {
    bool toggle = rotary_toggle_state.iterm_find_rotary_toggle_state;

    rotary_handler_state.turn_handler = (toggle)
        ? iterm_find_scroll_rotary
        : iterm_line_scroll_rotary;

    rotary_toggle_state.iterm_find_rotary_toggle_state = !toggle;
}

//
// ROTARY HANDLERS
//

void set_vim_search_rotary_handlers(void) {
    set_rotary_handlers(vim_search_rotary, vim_search_rotary_click_handler);
}

void set_vim_files_open_rotary_handlers(void) {
    set_rotary_handlers(main_up_down_rotary, vim_files_rotary_click_handler);
}

void set_vim_term_search_rotary_handlers(void) {
    set_rotary_handlers(main_up_down_rotary, vim_term_search_rotary_click_handler);
}

void set_vim_default_rotary_handlers(void) {
    set_rotary_handlers(vim_line_scroll_rotary, vim_default_rotary_click_handler);
}

void set_iterm_nav_rotary_handler(void) {
    set_rotary_handlers(iterm_line_scroll_rotary, iterm_nav_rotary_click_handler);
}

void set_iterm_find_rotary_handler(void) {
    set_rotary_handlers(iterm_find_scroll_rotary, noop_rotary);
}

void set_nav_default_rotary_handlers(void) {
    set_rotary_handlers(main_up_down_rotary, nav_scroll_rotary_click_handler);
}

void set_numbers_default_rotary_handler(void) {
    set_rotary_handlers(main_desktop_rotary, noop_rotary);
}

//
// LAYER
//

void layer_change_rotary_handler(Event event) {
    Layers layer = event.layer;
    switch(layer) {
        case NAV: set_nav_default_rotary_handlers(); break;
        case VIM: set_vim_default_rotary_handlers(); break;
        case NUMBERS: set_numbers_default_rotary_handler(); break;
        default: break;
    }
}

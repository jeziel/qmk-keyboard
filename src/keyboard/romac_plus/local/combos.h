#if defined(COMBO_ENABLE) && defined(CUSTOM_COMBOS)

#include QMK_KEYBOARD_H

enum combos {
  CMB_PLUS,
  CMB_MINS,
  CMB_BKSP,
  CMB_DOT,
  CMB_TAB,
  CMB_ENT,
  CMB_ESC,
};

const uint16_t PROGMEM plus_combo[] = {KC_4, KC_7, COMBO_END};
const uint16_t PROGMEM mins_combo[] = {KC_6, KC_9, COMBO_END};
const uint16_t PROGMEM bksp_combo[] = {KC_2, KC_3, COMBO_END};
const uint16_t PROGMEM dot_combo[] = {KC_5, KC_8, COMBO_END};
const uint16_t PROGMEM tab_combo[] = {KC_4, KC_5, COMBO_END};
const uint16_t PROGMEM ent_combo[] = {KC_5, KC_6, COMBO_END};
const uint16_t PROGMEM esc_combo[] = {KC_4, KC_6, COMBO_END};

combo_t key_combos[] = {
  [CMB_PLUS] = COMBO(plus_combo, KC_PLUS),
  [CMB_MINS] = COMBO(mins_combo, KC_MINS),
  [CMB_BKSP] = COMBO(bksp_combo, KC_BSPC),
  [CMB_DOT] = COMBO(dot_combo, KC_DOT),
  [CMB_TAB] = COMBO(tab_combo, KC_TAB),
  [CMB_ENT] = COMBO(ent_combo, KC_ENT),
  [CMB_ESC] = COMBO(esc_combo, KC_ESC),
};

#endif

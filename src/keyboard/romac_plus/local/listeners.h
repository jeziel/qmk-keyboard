#ifdef EVENTS_ENABLED

void layer_event_listener(Event event) {
    if (event.event_type != LAYER_CHANGE) {
        return;
    }

    uint8_t new_layer = event.layer;
    layer_off(current_layer);
    layer_on(new_layer);
    current_layer = new_layer;
}

void keyboard_event_listener(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case VIM_SEARCH: vim_find(); break;
        case VIM_SEARCH_CLEAR: vim_find_clear(); break;
        case VIM_TERM_SEARCH: vim_term_search(); break;
        case VIM_TERM_SEARCH_CLEAR: vim_escape(); break;
        case VIM_EASY_MOTION: vim_easymotion(); break;
        case VIM_MARK_SET: vim_mark_set(); break;
        case VIM_MARK_GO: vim_mark_go(); break;
        case VIM_FILES_OPEN: vim_fzf(); break;
        case VIM_FILES_CLOSE: vim_escape(); break;
        case VIM_TREE: vim_tree(); break;
        default: break;
    }
}

void primary_rotary_event_listener(Event event) {
    EventType event_type = event.event_type;

    if (event.rotary_position == PRIMARY_ROTARY) {
        // Encoder specific events
        switch(event_type) {
            case ROTARY_CW:
            case ROTARY_CCW: rotary_handler_state.turn_handler(event); break;
            case ROTARY_CLICK: rotary_handler_state.click_handler(event); break;
            default: break;
        }

        return;
    }

    switch(event_type) {
        case LAYER_CHANGE: layer_change_rotary_handler(event); break;
        case VIM_SEARCH: set_vim_search_rotary_handlers(); break;
        case VIM_FILES_OPEN: set_vim_files_open_rotary_handlers(); break;
        case VIM_TERM_SEARCH: set_vim_term_search_rotary_handlers(); break;
        case ITM_SCROLL: set_iterm_nav_rotary_handler(); break;
        case ITM_FIND: set_iterm_find_rotary_handler(); break;
        case VIM_SEARCH_CLEAR:
        case VIM_FILES_CLOSE:
        case VIM_TERM_SEARCH_CLEAR: set_vim_default_rotary_handlers(); break;
        default: break;
    }
}

void oled_event_listener(Event event) {
    EventType event_type = event.event_type;

    switch(event_type) {
        case LAYER_CHANGE: layer_change_oled_handler(event); break;
        default: break;
    }
}

void _emit_event_multiple(Event event) {
    layer_event_listener(event);
    keyboard_event_listener(event);
    primary_rotary_event_listener(event);
    oled_event_listener(event);
}

#endif

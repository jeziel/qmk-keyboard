/* Copyright 2018 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "jeziel.h"

typedef enum {
    NAV=0,
    VIM,
    NUMBERS,
} Layers;
Layers current_layer = NAV;

#include "local/main.h"

enum custom_keycodes {
    CT_VEZM = SAFE_RANGE,
    CT_VTRE,
    CT_ENC1,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [NAV] = LAYOUT(
        TAB_LEFT, KC_UP,     TAB_RIGHT,
        KC_LEFT,  LC_DOWN,   KC_RIGHT, 
        DN_HMND,  KC_SPC,    KC_BSPC,
        CT_ENC1,  CLOSE_TAB, DN_LYR
    ),

    [VIM] = LAYOUT(
        XXXXXXX, CT_VEZM, DN_VSCH,
        XXXXXXX, DN_VMRK, DN_VTSH,
        DN_ITMN, CT_VTRE, DN_VFLS,
        _______, XXXXXXX, _______
    ),

    [NUMBERS] = LAYOUT(
        KC_7,    KC_8,    KC_9,
        KC_4,    KC_5,    KC_6,
        KC_1,    KC_2,    KC_3,
        _______, KC_0,    _______
    ),

};


//
// QMK specific
//

void keyboard_post_init_user(void) {
    emit_layer_event(current_layer);
    oled_set_brightness(1);
}

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index != PRIMARY_ROTARY) { return true; }

#ifdef EVENTS_ENABLED
    emit_rotary_event((clockwise) ? ROTARY_CW : ROTARY_CCW, PRIMARY_ROTARY);
#endif

    return true;
}
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (!record->event.pressed) {
        // TODO. somehow figure out how to handle holds.
        return true;
    }

    switch(keycode) {
#ifdef EVENTS_ENABLED
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_ENC1: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
#endif
        default: break;
    }

    return true;
};


RotaryEventHandler current_primary_rotary_turn_handler = noop_rotary;
RotaryEventHandler current_primary_rotary_click_handler = noop_rotary;

//
// MOZ
//

typedef struct {
    bool tab_rotary_toggle_state;
    bool moz_search_toggle_state;
} MozLayerState;
MozLayerState moz_layer_state;

void set_moz_default_rotary_state(void) {
    moz_layer_state.tab_rotary_toggle_state = false;
    moz_layer_state.moz_search_toggle_state = false;
}

void moz_default_rotary_click_handler(Event event) {
    bool toggle = moz_layer_state.tab_rotary_toggle_state;

    current_primary_rotary_turn_handler = (toggle)
        ? moz_tab_scroll_rotary
        : moz_soft_scroll_rotary;

    moz_layer_state.tab_rotary_toggle_state = !toggle;
}

void moz_search_rotary_click_handler(Event event) {
    bool toggle = moz_layer_state.moz_search_toggle_state;

    current_primary_rotary_turn_handler = (toggle)
        ? moz_search_rotary
        : moz_soft_scroll_rotary;

    moz_layer_state.moz_search_toggle_state = !toggle;
}

//
// VIM
//

typedef struct {
    bool search_rotary_toggle_state;
    bool default_rotary_toggle_state;
} VimLayerState;
VimLayerState vim_layer_state;

void set_vim_defalt_rotary_state(void) {
    vim_layer_state.search_rotary_toggle_state = false;
    vim_layer_state.default_rotary_toggle_state = false;
}

void vim_default_rotary_click_handler(Event event) {
    bool toggle = vim_layer_state.default_rotary_toggle_state;

    current_primary_rotary_turn_handler = (toggle)
        ? vim_line_scroll_rotary
        : vim_block_scroll_rotary;

    vim_layer_state.default_rotary_toggle_state = !toggle;
}

void vim_search_rotary_click_handler(Event event) {
    bool toggle = vim_layer_state.search_rotary_toggle_state;

    current_primary_rotary_turn_handler = (toggle)
        ? vim_search_rotary
        : vim_line_scroll_rotary;

    vim_layer_state.search_rotary_toggle_state = !toggle;
}

//
// LAYER
//

void main_default_rotary_setup(void) {
   current_primary_rotary_turn_handler = main_desktop_rotary;
   current_primary_rotary_click_handler = noop_rotary;
}

void vim_default_rotary_setup(void) {
   current_primary_rotary_turn_handler = vim_line_scroll_rotary;
   current_primary_rotary_click_handler = vim_default_rotary_click_handler;
   set_vim_defalt_rotary_state();
}

void moz_default_rotary_setup(void) {
   current_primary_rotary_turn_handler = moz_tab_scroll_rotary;
   current_primary_rotary_click_handler = moz_default_rotary_click_handler;
   set_moz_default_rotary_state();
}


#ifdef EVENTS_ENABLED

enum layers current_layer = MAIN;

void layer_event_listener(Event event) {
    if (event.event_type != LAYER_CHANGE) {
        return;
    }

    uint8_t new_layer = event.layer;
    layer_off(current_layer);
    layer_on(new_layer);
    current_layer = new_layer;
}

void layer_change_rotary_handler(Event event) {
    uint8_t layer = event.layer;
    switch(layer) {
        case MAIN: main_default_rotary_setup(); break;
        case VIM: vim_default_rotary_setup(); break;
        case MOZ: moz_default_rotary_setup(); break;
        default: break;
    }
}

void keyboard_event_listener(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case VIM_SEARCH: vim_find(); break;
        case VIM_SEARCH_CLEAR: vim_find_clear(); break;
        case VIM_TERM_SEARCH: vim_term_search(); break;
        case VIM_TERM_SEARCH_CLEAR: vim_escape(); break;
        case VIM_EASY_MOTION: vim_easymotion(); break;
        case VIM_MARK_SET: vim_mark_set(); break;
        case VIM_MARK_GO: vim_mark_go(); break;
        case VIM_FILES_OPEN: vim_fzf(); break;
        case VIM_FILES_CLOSE: vim_escape(); break;
        case VIM_TREE: vim_tree(); break;
        case MOZ_TERM_SEARCH: moz_term_search(); break;
        case MOZ_SEARCH: moz_search(); break;
        case MOZ_SEARCH_CLEAR: moz_search_clear(); break;
        default: break;
    }
}

void primary_rotary_event_listener(Event event) {
    EventType event_type = event.event_type;

    if (event.rotary_position == PRIMARY_ROTARY) {
        // Encoder specific events
        switch(event_type) {
            case ROTARY_CW:
            case ROTARY_CCW: current_primary_rotary_turn_handler(event); break;
            case ROTARY_CLICK: current_primary_rotary_click_handler(event); break;
            default: break;
        }

        return;
    }

    switch(event_type) {
        case LAYER_CHANGE: layer_change_rotary_handler(event); break;
        case VIM_SEARCH:
           current_primary_rotary_turn_handler = vim_search_rotary;
           current_primary_rotary_click_handler = vim_search_rotary_click_handler;
           break;
        case VIM_TERM_SEARCH:
           current_primary_rotary_turn_handler = main_up_down_rotary;
           current_primary_rotary_click_handler = vim_term_search_rotary_click_handler;
           break;
        case VIM_FILES_OPEN:
           current_primary_rotary_turn_handler = main_up_down_rotary;
           current_primary_rotary_click_handler = vim_files_rotary_click_handler;
           break;
        case VIM_SEARCH_CLEAR:
        case VIM_TERM_SEARCH_CLEAR:
        case VIM_FILES_CLOSE:
           vim_default_rotary_setup(); break;
        case MOZ_SEARCH:
           current_primary_rotary_turn_handler = moz_search_rotary;
           current_primary_rotary_click_handler = moz_search_rotary_click_handler;
           break;
        case MOZ_SEARCH_CLEAR: moz_default_rotary_setup(); break;
        default: break;
    }
}

void secondary_rotary_event_listener(Event event) {
    // TODO. 2nd rotary too much rotary?
}

void _emit_event_multiple(Event event) {
    layer_event_listener(event);
    keyboard_event_listener(event);
    primary_rotary_event_listener(event);
    secondary_rotary_event_listener(event);

    /*
     * TODO. I used to have this declared as an array, but since the amount of
     * listeners wouldn't be dynamic for a keyboard, I decided to leave it like
     * this and have every keyboard define what to call.

    ComponentListener listeners[] = {
        listener1,
        listener2,
        ...
    };
    uint8_t total_listeners = sizeof(listeners)/sizeof(listeners[0]);
     
    for (uint8_t i=0; i < total_listeners; i++) {
        listeners[i](event);
    }
    */
}

#endif

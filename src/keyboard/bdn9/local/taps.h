#if defined(TAP_DANCE_ENABLE)

void dance_layer (qk_tap_dance_state_t *state, void *user_data);

enum dances {
    TD_LAYER_SELECTION,
    TD_BROWSER_NAV,
    TD_BROWSER_TAB_NAV,
    TD_VIM_SEARCH,
    TD_VIM_TERM_SEARCH,
    TD_VIM_MARK,
    TD_VIM_FILES,
    TD_MOZ_SEARCH,
};

#define DN_LAYER           TD(TD_LAYER_SELECTION)
#define DN_BROWSER_NAV     TD(TD_BROWSER_NAV)
#define DN_BROWSER_TAB_NAV TD(TD_BROWSER_TAB_NAV)
#define DN_VIM_SEARCH      TD(TD_VIM_SEARCH)
#define DN_VIM_TERM_SEARCH TD(TD_VIM_TERM_SEARCH)
#define DN_VIM_MARK        TD(TD_VIM_MARK)
#define DN_VIM_FILES       TD(TD_VIM_FILES)
#define DN_MOZ_SEARCH      TD(TD_MOZ_SEARCH)

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_LAYER_SELECTION] = ACTION_TAP_DANCE_FN(dance_layer),
    [TD_BROWSER_NAV]     = ACTION_TAP_DANCE_DOUBLE(BACK_PAGE, FORWARD_PAGE),
    [TD_BROWSER_TAB_NAV] = ACTION_TAP_DANCE_DOUBLE(FIRST_TAB, LAST_TAB),
    [TD_VIM_TERM_SEARCH] = ACTION_DOUBLE_TAP_EVENT(VIM_TERM_SEARCH, VIM_TERM_SEARCH_CLEAR),
    [TD_VIM_SEARCH]      = ACTION_DOUBLE_TAP_EVENT(VIM_SEARCH, VIM_SEARCH_CLEAR),
    [TD_VIM_MARK]        = ACTION_DOUBLE_TAP_EVENT(VIM_MARK_SET, VIM_MARK_GO),
    [TD_VIM_FILES]       = ACTION_DOUBLE_TAP_EVENT(VIM_FILES_OPEN, VIM_FILES_CLOSE),
    [TD_MOZ_SEARCH]      = ACTION_DOUBLE_TAP_EVENT(MOZ_SEARCH, MOZ_SEARCH_CLEAR),
};

void dance_layer(qk_tap_dance_state_t *state, void *user_data) {
    switch (state->count) {
        case 2: emit_layer_event(VIM); break;
        case 3: emit_layer_event(MOZ); break;
        default: emit_layer_event(MAIN); break;
    }

    reset_tap_dance(state);
}

#endif

/* Copyright 2019 Danny Nguyen <danny@keeb.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

#include "jeziel.h"

enum layers {
    MAIN=0,
    VIM,
    MOZ,
};

#include "local/main.h"

enum custom_keycodes {
    CT_VIM_EASY = SAFE_RANGE,
    CT_MOZ_TERM_SEARCH,
    CT_VIM_TREE,
    CT_ENC1,
    CT_ENC2,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [MAIN] = LAYOUT(
            CT_ENC1, DN_LAYER, CT_ENC2,
            XXXXXXX, XXXXXXX,  XXXXXXX,
            XXXXXXX, XXXXXXX,  XXXXXXX
            ),

    [VIM] = LAYOUT(
            _______,      _______,            _______,
            CT_VIM_TREE,  DN_VIM_TERM_SEARCH, CT_VIM_EASY,
            DN_VIM_FILES, DN_VIM_SEARCH,      DN_VIM_MARK
            ),

    [MOZ] = LAYOUT(
            _______,            _______,            _______,
            DN_BROWSER_NAV,     CT_MOZ_TERM_SEARCH, XXXXXXX,
            DN_BROWSER_TAB_NAV, DN_MOZ_SEARCH,      XXXXXXX
            ),
};

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index > SECONDARY_ROTARY) {
        return true;
    }

#ifdef EVENTS_ENABLED
    emit_rotary_event((clockwise) ? ROTARY_CW : ROTARY_CCW, index);
#endif

    return true;
}
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (!record->event.pressed) {
        // TODO. somehow figure out how to handle holds.
        return true;
    }

    switch(keycode) {
#ifdef EVENTS_ENABLED
        case CT_VIM_EASY: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VIM_TREE: emit_key_event(VIM_TREE); break;
        case CT_MOZ_TERM_SEARCH: emit_key_event(MOZ_TERM_SEARCH); break;
        case CT_ENC1: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
        case CT_ENC2: emit_rotary_event(ROTARY_CLICK, SECONDARY_ROTARY); break;
#endif
        default: break;
    }

    return true;
};



void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_layer_event(MAIN);
#endif
}

/*
 * TODO. to implement later, this basically let you scroll through the open
 * apps with a alt-tab alt-shft-tab.

    register_code(KC_LCMD);
    tap_code(KC_TAB);
    unregister_code(KC_LCMD);

 */

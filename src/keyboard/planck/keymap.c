/* Copyright 2015-2017 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "jeziel.h"

typedef enum {
    _MAIN=0,
    _TRR,
    _SC,
    _LOWER,
    _SC_T,
    _T_WP,
    _LMB,
} Layers;

#include "local/main.h"
enum custom_keycodes {
    CT_ENC = SAFE_RANGE,
    CT_VTRE,
    CT_VEZM,
    CT_VGTD
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_MAIN] = LAYOUT_planck_grid(
        KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,    XXXXXXX, XXXXXXX, KC_F,   KC_G,    KC_C,    KC_R,    KC_L,
        KC_A,    KC_O,    KC_E,    KC_U,    KC_I,    XXXXXXX, XXXXXXX, KC_D,   KC_H,    KC_T,    KC_N,    KC_S,
        LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    XXXXXXX, XXXXXXX, KC_B,   KC_M,    KC_W,    KC_V,    LS_Z,
        XXXXXXX, XXXXXXX, KC_LALT, KC_LCMD, LOWER,   XXXXXXX, XXXXXXX, KC_SPC, KC_RCTL, XXXXXXX, XXXXXXX, CT_ENC
    ),

    [_TRR] = LAYOUT_planck_grid(
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, KC_LCTL, MO_TRR,  KC_SPC,  _______, _______, _______, _______, _______, _______, GT_TRR
    ),

    [_SC] = LAYOUT_planck_grid(
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, KC_LCTL, MO_TEAM, KC_SPC,  _______, _______, _______, _______, _______, _______, GT_SC
    ),

    [_LOWER] = LAYOUT_planck_grid(
        DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB, _______, _______, DN_VSCH, KC_7,    KC_8,    KC_9,    CT_VGTD,
        KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,   _______, _______, DN_VTSH, KC_4,    KC_5,    KC_6,    KC_0,
        GT_SC,   GT_TRR,   GT_LMB,  XXXXXXX,   KC_PGDN,   _______, _______, DN_VMRK, KC_1,    KC_2,    KC_3,    XXXXXXX,
        _______, _______,  _______, _______,   _______,   _______, _______, DN_VFLS, CT_VTRE, CT_VEZM, _______, _______
    ),

    [_SC_T] = LAYOUT_planck_grid(
        KC_LCTL,  DN_TM1,  DN_TM2,  DN_TM3,  _______, _______, _______, _______, _______, _______, _______, _______,
        KC_LSFT,  DN_TM4,  DN_TM5,  DN_TM6,  _______, _______, _______, _______, _______, _______, _______, _______,
        KC_LALT,  DN_TM7,  DN_TM8,  DN_TM9,  _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

    [_T_WP] = LAYOUT_planck_grid(
        KC_1,    KC_3,    KC_5,    KC_7,    KC_9,    _______, _______, _______, _______, _______, _______, _______,
        KC_2,    KC_4,    KC_6,    KC_8,    KC_0,    _______, _______, _______, _______, _______, _______, _______,
        KC_F1,   KC_F2,   KC_F3,   _______, _______, _______, _______, _______, _______, _______, _______, _______,
        XXXXXXX, XXXXXXX, KC_LCMD, MO_TRR,  KC_SPC,  _______, _______, _______, _______, _______, _______, _______
    ),

    [_LMB] = LAYOUT_planck_grid(
        KC_ESC,  XXXXXXX, KC_UP,   XXXXXXX,  XXXXXXX, _______, _______, _______, _______, _______, _______, _______, 
        XXXXXXX, KC_LEFT, KC_DOWN, KC_RIGHT, XXXXXXX, _______, _______, _______, _______, _______, _______, _______,
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX, _______, _______, _______, _______, _______, _______, _______,
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,  KC_LALT, _______, _______, _______, _______, _______, _______, GT_LMB
    )
};

#ifdef RGB_MATRIX_ENABLE
void rgb_matrix_indicators_user(void) {
  if (g_suspend_state || keyboard_config.disable_layer_led) { return; }

  int current_layer = biton32(layer_state);
  int selected_layer = (current_layer < _LOWER) ? _MAIN : current_layer;

  set_layer_color(selected_layer);
  set_encoder_action_light();
}
#endif

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
#ifdef EVENTS_ENABLED
    emit_rotary_event((clockwise) ? ROTARY_CW : ROTARY_CCW, index);
#endif

    return false;
}
#endif

void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_key_event(MAIN_SCROLL);
#endif
}

bool process_record_user_pressed(uint16_t keycode) {
    switch(keycode) {
#ifdef EVENTS_ENABLED
        case CT_ENC: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VGTD: emit_key_event(VIM_GOTO_DEFINITION); break;
#endif
        default: break;
    }

    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        return process_record_user_pressed(keycode);
    }

    return true;
};


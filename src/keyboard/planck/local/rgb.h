#ifdef RGB_MATRIX_ENABLE

#include "layers.h"

void set_layer_color(int layer);
void set_encoder_action_light(void);

#define RGB_OFF {0, 0, 0}
#define DARK_CL {RGB_RED}
#define ROTARY_ACTION_KEY 45
#define LEAD_CL {RGB_CORAL}

enum _rgb{_R, _G, _B};

const uint8_t PROGMEM ledmap[][DRIVER_LED_TOTAL][3] = {
    [_DVORAK] = {
        RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, RGB_OFF, RGB_OFF, LEAD_CL, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, LEAD_CL, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF
    },

    /*
     * RGB_TEAL and RGB_CYAN are almost the same.
     * RGB_TURQUOISE and RGB_AZURE are almost the same.
     * RGB_CORAL and RGB_PINK are almost the same.
     */
    [_LOWER] = {
        RGB_OFF, RGB_OFF, DARK_CL, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, DARK_CL, DARK_CL, DARK_CL, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, LEAD_CL, RGB_OFF, RGB_OFF, RGB_OFF,
        RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, RGB_OFF, DARK_CL
    },
};

void set_encoder_action_light() {
    int current_layer = biton32(layer_state);
    switch(current_layer) {
        case _VIM:
            rgb_matrix_set_color(ROTARY_ACTION_KEY, RGB_GREEN);
            break;
        case _MOZ:
            rgb_matrix_set_color(ROTARY_ACTION_KEY, RGB_ORANGE);
            break;
        default:
            rgb_matrix_set_color(ROTARY_ACTION_KEY, 0, 0, 0);
    }
}

void set_layer_color(int layer) {
  float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
  for (int i = 0; i < DRIVER_LED_TOTAL; i++) {
    float r = ledmap[layer][i][_R] * f;
    float g = ledmap[layer][i][_G] * f;
    float b = ledmap[layer][i][_B] * f;
    rgb_matrix_set_color( i, r, g, b );
  }
}

#endif

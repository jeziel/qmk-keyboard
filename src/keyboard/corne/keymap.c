/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H
#include "jeziel.h"

enum layers {
    _MAIN,
    _TRR,
    _SC,
    _LOWER,
    _SC_T,
    _T_WP,
};

#include "local/main.h"

enum custom_keycodes {
    CT_ENC2 = SAFE_RANGE,
    CT_VTRE,
	CT_VEZM,
	CT_VGTD
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_MAIN] = LAYOUT_split_3x6_3(
      XXXXXXX,    KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,  /* | */   KC_F,    KC_G,    KC_C,    KC_R,    KC_L, XXXXXXX,
      XXXXXXX,    KC_A,    KC_O,    R2_C1,   KC_U,    KC_I,  /* | */   KC_D,    KC_H,    KC_T,    KC_N,    KC_S, XXXXXXX,
      XXXXXXX,    LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,  /* | */   KC_B,    KC_M,    KC_W,    KC_V,    LS_Z, XXXXXXX,
                                    KC_LALT, KC_LCMD, LOWER, /* | */   KC_SPC,  KC_LCTL, XXXXXXX
  ),

  [_LOWER] = LAYOUT_split_3x6_3(
      XXXXXXX, DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB, /* | */ DN_VSCH, KC_7,    KC_8,   KC_9, CT_VGTD, XXXXXXX,
      XXXXXXX, KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,   /* | */ DN_VTSH, KC_4,    KC_5,   KC_6, KC_0,    XXXXXXX,
      XXXXXXX, GT_SC,   GT_TRR,   XXXXXXX, XXXXXXX,   KC_PGDN,   /* | */ DN_VMRK, KC_1,    KC_2,   KC_3, XXXXXXX, XXXXXXX,
                                  _______, _______,   XXXXXXX,   /* | */ DN_VFLS, CT_VTRE, CT_VEZM
  ),
};

void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_key_event(MAIN_SCROLL);
#endif
}

bool process_record_user_pressed(uint16_t keycode) {
    switch(keycode) {
#ifdef EVENTS_ENABLED
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_VGTD: emit_key_event(VIM_GOTO_DEFINITION); break;
#endif
        default: break;
    }

    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        return process_record_user_pressed(keycode);
    }

    return true;
};

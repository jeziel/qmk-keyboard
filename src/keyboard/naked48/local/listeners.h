#ifdef EVENTS_ENABLED

#include QMK_KEYBOARD_H

#ifdef ROTARY_ENABLED
void rotary_event_listener(Event event) {
    EventType event_type = event.event_type;

    if (event.rotary_position == PRIMARY_ROTARY) {
        // Encoder specific events
        switch(event_type) {
            case ROTARY_CW:
            case ROTARY_CCW: rotary_turn_handler(event); break;
            case ROTARY_CLICK: rotary_click_handler(event); break;
            default: break;
        }

        return;
    }

    switch(event_type) {
        case MAIN_SCROLL: set_main_default_rotary_handlers(event); break;
        case VIM_SEARCH: set_vim_search_rotary_handlers(event); break;
        case VIM_TERM_SEARCH: set_vim_term_search_rotary_handlers(event); break;
        case VIM_FILES_OPEN: set_vim_files_open_rotary_handlers(event); break;
        case VIM_SEARCH_CLEAR:
        case VIM_TERM_SEARCH_CLEAR:
        case VIM_FILES_CLOSE:
           set_vim_default_rotary_handlers(event); break;
        case ITM_SCROLL: set_iterm_scroll_rotary_handlers(event); break;
        case VIM_GOTO_DEFINITION: set_vim_jump_rotary_handlers(event); break;
        default: break;
    }
}
#endif

void keyboard_event_listener(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case VIM_SEARCH: vim_find(); break;
        case VIM_SEARCH_CLEAR: vim_find_clear(); break;
        case VIM_TERM_SEARCH: vim_term_search(); break;
        case VIM_TERM_SEARCH_CLEAR: vim_escape(); break;
        case VIM_EASY_MOTION: vim_easymotion(); break;
        case VIM_MARK_SET: vim_mark_set(); break;
        case VIM_MARK_GO: vim_mark_go(); break;
        case VIM_FILES_OPEN: vim_fzf(); break;
        case VIM_FILES_CLOSE: vim_escape(); break;
        case VIM_TREE: vim_tree(); break;
        case VIM_GOTO_DEFINITION: vim_ale_goto_definition(); break;
        default: break;
    }
}

void _emit_event_multiple(Event event) {
    keyboard_event_listener(event);

#ifdef ROTARY_ENABLED
    rotary_event_listener(event);
#endif
}

#endif

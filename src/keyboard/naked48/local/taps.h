#ifdef TAP_DANCE_ENABLE

#include QMK_KEYBOARD_H

void dance_layer(qk_tap_dance_state_t *state, void *user_data) {
    switch (state->count) {
        case 2: emit_layer_event(VIM); break;
        case 3: emit_layer_event(NUMBERS); break;
        default: emit_layer_event(NAV); break;
    }

    reset_tap_dance(state);
}

enum dances {
    TD_VIM_SEARCH,
    TD_VIM_TERM_SEARCH,
    TD_VIM_MARK,
    TD_VIM_FILES,
    TD_GRL_HME_END,
    TD_DEF_SCROLL,

    TD_TEAM_1,
    TD_TEAM_2,
    TD_TEAM_3,
    TD_TEAM_4,
    TD_TEAM_5,
    TD_TEAM_6,
    TD_TEAM_7,
    TD_TEAM_8,
    TD_TEAM_9,

    TD_VIEW_1,
    TD_VIEW_2,
    TD_VIEW_3,
};

#define DN_HMND TD(TD_GRL_HME_END)
#define DN_VSCH TD(TD_VIM_SEARCH)
#define DN_VTSH TD(TD_VIM_TERM_SEARCH)
#define DN_VMRK TD(TD_VIM_MARK)
#define DN_VFLS TD(TD_VIM_FILES)
#define DN_SCRL TD(TD_DEF_SCROLL)

#define DN_TM1 TD(TD_TEAM_1)
#define DN_TM2 TD(TD_TEAM_2)
#define DN_TM3 TD(TD_TEAM_3)
#define DN_TM4 TD(TD_TEAM_4)
#define DN_TM5 TD(TD_TEAM_5)
#define DN_TM6 TD(TD_TEAM_6)
#define DN_TM7 TD(TD_TEAM_7)
#define DN_TM8 TD(TD_TEAM_8)
#define DN_TM9 TD(TD_TEAM_9)

#define DN_VW1 TD(TD_VIEW_1)
#define DN_VW2 TD(TD_VIEW_2)
#define DN_VW3 TD(TD_VIEW_3)

tap_dance_action_t tap_dance_actions[] = {
    [TD_VIM_TERM_SEARCH] = ACTION_DOUBLE_TAP_EVENT(VIM_TERM_SEARCH, VIM_TERM_SEARCH_CLEAR),
    [TD_VIM_SEARCH]      = ACTION_DOUBLE_TAP_EVENT(VIM_SEARCH, VIM_SEARCH_CLEAR),
    [TD_VIM_MARK]        = ACTION_DOUBLE_TAP_EVENT(VIM_MARK_SET, VIM_MARK_GO),
    [TD_VIM_FILES]       = ACTION_DOUBLE_TAP_EVENT(VIM_FILES_OPEN, VIM_FILES_CLOSE),
    [TD_GRL_HME_END]     = ACTION_TAP_DANCE_DOUBLE(KC_HOME, KC_END),
    [TD_DEF_SCROLL]      = ACTION_DOUBLE_TAP_EVENT(MAIN_SCROLL, ITM_SCROLL),

    [TD_TEAM_1] = TEAM_TAP(KC_1, team_attack),
    [TD_TEAM_2] = TEAM_TAP(KC_2, team_move),
    [TD_TEAM_3] = TEAM_TAP(KC_3, team_attack),
    [TD_TEAM_4] = TEAM_TAP(KC_4, team_attack),
    [TD_TEAM_5] = TEAM_TAP(KC_5, team_move),
    [TD_TEAM_6] = TEAM_TAP(KC_6, team_attack),
    [TD_TEAM_7] = TEAM_TAP(KC_7, team_attack),
    [TD_TEAM_8] = TEAM_TAP(KC_8, team_attack),
    [TD_TEAM_9] = TEAM_TAP(KC_9, team_attack),

    [TD_VIEW_1] = VIEW_TAP(SC_VIEW_1),
    [TD_VIEW_2] = VIEW_TAP(SC_VIEW_2),
    [TD_VIEW_3] = VIEW_TAP(SC_VIEW_3),
};

#endif

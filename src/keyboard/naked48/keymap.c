#include QMK_KEYBOARD_H
#include "jeziel.h"

enum layers {
    _MAIN = 0,
    _TRR,
    _SC,
    _LOWER,
    _SC_T,
    _T_WP,
};

#include "local/main.h"

enum custom_keycodes {
    CT_VTRE = SAFE_RANGE,
    CT_VEZM,
    CT_VGTD
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_MAIN] = LAYOUT(
       XXXXXXX, KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,                   KC_F,    KC_G,    KC_C,    KC_R,    KC_L,   XXXXXXX,
       XXXXXXX, KC_A,    KC_O,    KC_E,    KC_U,    KC_I,                   KC_D,    KC_H,    KC_T,    KC_N,    KC_S,   XXXXXXX,
       XXXXXXX, LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,                   KC_B,    KC_M,    KC_W,    KC_V,    LS_Z,   XXXXXXX,
                XXXXXXX, XXXXXXX, KC_LALT, KC_LCMD, LOWER, FK_RCCW, FK_RCW, KC_SPC,  KC_RCTL, XXXXXXX, XXXXXXX, XXXXXXX
  ),

  [_TRR] = LAYOUT(
       _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
       _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
       _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
                _______, _______, KC_LCMD, MO_TRR,  KC_SPC,  _______, _______, _______, _______, GT_TRR,  _______, _______
  ),

  [_SC] = LAYOUT(
      _______, _______,  _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
      _______, _______,  _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
      _______, _______,  _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______,
               _______,  _______, KC_LCTL, MO_TEAM, KC_SPC,  _______, _______, _______, _______, GT_SC,   _______, _______
  ),

  [_LOWER] = LAYOUT(
      XXXXXXX, DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB,                 DN_VSCH, KC_7,    KC_8,    KC_9,    CT_VGTD, XXXXXXX,
      XXXXXXX, KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,                   DN_VTSH, KC_4,    KC_5,    KC_6,    KC_0,    XXXXXXX,
      XXXXXXX, GT_SC,   GT_TRR,   XXXXXXX, XXXXXXX,   KC_PGDN,                   DN_VMRK, KC_1,    KC_2,    KC_3,    XXXXXXX, XXXXXXX,
               _______, _______,  _______, _______,   _______, _______, _______, DN_VFLS, CT_VTRE, CT_VEZM, XXXXXXX, XXXXXXX
  ),


  [_SC_T] = LAYOUT(
      XXXXXXX, _______,  DN_TM1,  DN_TM2,  DN_TM3,  _______,                    _______, _______, _______, _______, _______, XXXXXXX,
      XXXXXXX, _______,  DN_TM4,  DN_TM5,  DN_TM6,  _______,                    _______, _______, _______, _______, _______, XXXXXXX,
      XXXXXXX, _______,  DN_TM7,  DN_TM8,  DN_TM9,  _______,                    _______, _______, _______, _______, _______, XXXXXXX,
               _______,  _______, _______, _______, _______,  _______, _______, _______, _______, _______, _______, _______
  ),

  [_T_WP] = LAYOUT(
      XXXXXXX, KC_1,     KC_3,    KC_5,    KC_7,    KC_9,                      _______, _______, _______, _______, _______, XXXXXXX,
      XXXXXXX, KC_2,     KC_4,    KC_6,    KC_8,    KC_0,                      _______, _______, _______, _______, _______, XXXXXXX,
      XXXXXXX, KC_F1,    KC_F2,   KC_F3,   XXXXXXX, XXXXXXX,                   _______, _______, _______, _______, _______, XXXXXXX,
               _______,  _______, KC_LCMD, MO_TRR,  KC_SPC,  _______, _______, _______, _______, _______, _______, _______
  )
};


void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_key_event(MAIN_SCROLL);
#endif
}

bool process_record_user_pressed(uint16_t keycode) {
    switch(keycode) {
        case FK_RCCW: emit_rotary_event(ROTARY_CCW, PRIMARY_ROTARY); break;
        case FK_RCW:  emit_rotary_event(ROTARY_CW, PRIMARY_ROTARY); break;
        case FK_RCLK: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_VGTD: emit_key_event(VIM_GOTO_DEFINITION); break;
        default: break;
    }

    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef EVENTS_ENABLED
    if (record->event.pressed) {
        return process_record_user_pressed(keycode);
    }
#endif

    return true;
};

MOUSEKEY_ENABLE = yes        # Mouse keys
TAP_DANCE_ENABLE = yes
COMBO_ENABLE = yes
ENCODER_ENABLE = yes

RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
OLED_ENABLE = yes
OLED_DRIVER = SSD1306
LTO_ENABLE = yes

# If you want to change the display of OLED, you need to change here
# SRC +=  ./common/oled_helper.c \


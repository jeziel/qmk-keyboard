#ifdef OLED_ENABLE

#include QMK_KEYBOARD_H

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    oled_scroll_set_area(1, 2);
    return OLED_ROTATION_0;
}

typedef struct {
    uint8_t x1;
    uint8_t y1;
    uint8_t x2;
    uint8_t y2;
} WindowCoordinates;


const static WindowCoordinates coordinates = {
    4, 4,
    124, 28,
};

bool activate_pixel(void) {
    const uint8_t CHANCE_ON = 3;
    uint8_t selection = rand() % 10;

    return selection < CHANCE_ON;
}

void starry_night(void) {
    uint8_t x = rand() % OLED_DISPLAY_WIDTH;
    uint8_t y = rand() % OLED_DISPLAY_HEIGHT;

    bool good_top = x < coordinates.x1 || y < coordinates.y1;
    bool good_bottom = x > coordinates.x2 || y > coordinates.y2 ;

    if (good_top || good_bottom) {
        oled_write_pixel(x, y, activate_pixel());
    }
}

bool oled_task_user(void) {
    // starry_night();
    // os_scroll_line_message();
    return false;
}

#endif

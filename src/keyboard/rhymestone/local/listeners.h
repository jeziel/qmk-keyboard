#ifdef EVENTS_ENABLED

void keyboard_event_listener(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case VIM_SEARCH: vim_find(); break;
        case VIM_SEARCH_CLEAR: vim_find_clear(); break;
        case VIM_TERM_SEARCH: vim_term_search(); break;
        case VIM_TERM_SEARCH_CLEAR: vim_escape(); break;
        case VIM_EASY_MOTION: vim_easymotion(); break;
        case VIM_MARK_SET: vim_mark_set(); break;
        case VIM_MARK_GO: vim_mark_go(); break;
        case VIM_FILES_OPEN: vim_fzf(); break;
        case VIM_FILES_CLOSE: vim_escape(); break;
        case VIM_TREE: vim_tree(); break;
        case VIM_GOTO_DEFINITION: vim_ale_goto_definition(); break;
        default: break;
    }
}

#ifdef ROTARY_ENABLED
void secondary_rotary_event_listener(Event event) {
    EventType event_type = event.event_type;

    if (event.rotary_position == SECONDARY_ROTARY) {
        // Encoder specific events
        switch(event_type) {
            case ROTARY_CW:
            case ROTARY_CCW: rotary_turn_handler(event); break;
            case ROTARY_CLICK: rotary_click_handler(event); break;
            default: break;
        }

        return;
    }

    switch(event_type) {
        case MAIN_SCROLL: set_main_default_rotary_handlers(event); break;
        case VIM_SEARCH: set_vim_search_rotary_handlers(event); break;
        case VIM_TERM_SEARCH: set_vim_term_search_rotary_handlers(event); break;
        case VIM_FILES_OPEN: set_vim_files_open_rotary_handlers(event); break;
        case VIM_SEARCH_CLEAR:
        case VIM_TERM_SEARCH_CLEAR:
        case VIM_FILES_CLOSE:
           set_vim_default_rotary_handlers(event); break;
        case ITM_SCROLL: set_iterm_scroll_rotary_handlers(event); break;
        case VIM_GOTO_DEFINITION: set_vim_jump_rotary_handlers(event); break;
        default: break;
    }
}
#endif

#ifdef OLED_ENABLE
void oled_event_listener(Event event) {
    EventType event_type = event.event_type;

    switch(event_type) {
        case ROTARY_UD:
        case MAIN_SCROLL: os_scroll_line_message(); break;
        case ROTARY_PGUD: os_scroll_page_message(); break;
        case ROTARY_ILSCR:
        case ITM_SCROLL: iterm_scroll_line_message(); break;
        case ROTARY_IPSCR: iterm_scroll_page_message(); break;
        case ROTARY_VSRCH:
        case VIM_SEARCH: vim_search_oled_setup(); break;
        case VIM_FILES_OPEN:
        case VIM_TERM_SEARCH: vim_up_down_oled_setup(); break;
        case ROTARY_VJUMP:
        case VIM_GOTO_DEFINITION: vim_jump_oled_setup(); break;
        case ROTARY_VPSCR: vim_scroll_page_message(); break;
        case ROTARY_VLSCR:
        case VIM_SEARCH_CLEAR:
        case VIM_TERM_SEARCH_CLEAR:
        case VIM_FILES_CLOSE:
           vim_scroll_line_message();
           break;
        default: break;
    }
}
#endif

void _emit_event_multiple(Event event) {
    keyboard_event_listener(event);
#ifdef ROTARY_ENABLED
    secondary_rotary_event_listener(event);
#endif
#ifdef OLED_ENABLE
    oled_event_listener(event);
#endif
}

#endif

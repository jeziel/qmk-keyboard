#ifdef ROTARY_ENABLED

//
// CLICK HANDLERS
//
void vim_scroll_rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    toggle_click_handler(&rotary_state->vim_scroll_rotary_toggle_state,
        &rotary_state->turn,
        vim_line_scroll_rotary_turn_handler,
        vim_block_scroll_rotary_turn_handler);
    emit_rotary_handler_event(
            event,
            rotary_state->vim_scroll_rotary_toggle_state,
            ROTARY_VPSCR,
            ROTARY_VLSCR);
}

void vim_search_rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    toggle_click_handler(&rotary_state->vim_search_rotary_toggle_state,
        &rotary_state->turn,
        vim_search_rotary_turn_handler,
        vim_line_scroll_rotary_turn_handler);
    emit_rotary_handler_event(
            event,
            rotary_state->vim_search_rotary_toggle_state,
            ROTARY_VLSCR,
            ROTARY_VSRCH);
}

void vim_jump_rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    toggle_click_handler(&rotary_state->vim_jump_rotary_toggle_state,
        &rotary_state->turn,
        vim_jump_rotary_turn_handler,
        vim_line_scroll_rotary_turn_handler);
    emit_rotary_handler_event(
            event,
            rotary_state->vim_jump_rotary_toggle_state,
            ROTARY_VLSCR,
            ROTARY_VJUMP);
}

void iterm_scroll_rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    toggle_click_handler(&rotary_state->iterm_scroll_rotary_toggle_state,
        &rotary_state->turn,
        iterm_line_scroll_rotary_turn_handler,
        iterm_block_scroll_rotary_turn_handler);
    emit_rotary_handler_event(
            event,
            rotary_state->iterm_scroll_rotary_toggle_state,
            ROTARY_IPSCR,
            ROTARY_ILSCR);
}

void main_scroll_rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    toggle_click_handler(&rotary_state->main_default_rotary_toggle_state,
        &rotary_state->turn,
        main_up_down_rotary_turn_handler,
        main_pgup_down_rotary_turn_handler);
    emit_rotary_handler_event(
            event,
            rotary_state->main_default_rotary_toggle_state,
            ROTARY_PGUD,
            ROTARY_UD);
}

//
// HANDLER DEFINITION.
//

void set_vim_search_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            vim_search_rotary_turn_handler,
            vim_search_rotary_click_handler);
}

void set_vim_term_search_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            main_up_down_rotary_turn_handler,
            vim_term_search_rotary_click_handler);
}

void set_vim_files_open_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            main_up_down_rotary_turn_handler,
            vim_files_rotary_click_handler);
}

void set_iterm_scroll_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            iterm_line_scroll_rotary_turn_handler,
            iterm_scroll_rotary_click_handler);
}

void set_vim_default_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            vim_line_scroll_rotary_turn_handler,
            vim_scroll_rotary_click_handler);
}

void set_main_default_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            main_up_down_rotary_turn_handler,
            main_scroll_rotary_click_handler);
}

void set_vim_jump_rotary_handlers(Event event) {
    set_rotary_handlers(
            event,
            vim_jump_rotary_turn_handler,
            vim_jump_rotary_click_handler);
}

#endif

#ifdef OLED_ENABLE

#include QMK_KEYBOARD_H

const static char PROGMEM os_line[] = "OS Scroll Line";
const static char PROGMEM os_page[] = "OS Scroll Page";
const static char PROGMEM iterm_line[] = "Iterm Scroll Line";
const static char PROGMEM iterm_page[] = "Iterm Scroll Page";
const static char PROGMEM vim_line[] = "VIM Scroll Line";
const static char PROGMEM vim_page[] = "VIM Scroll Page";
const static char PROGMEM vim_next_prev[] = "VIM Next/Prev";
const static char PROGMEM vim_up_down[] = "VIM Up/Down";
const static char PROGMEM vim_jump[] = "VIM Jump";
const static char PROGMEM layer_main[] = "Main";
const static char PROGMEM layer_terraria[] = "Terraria";
const static char PROGMEM layer_starcraft[] = "Starcraft";

void write_line(const char *message) {
    oled_set_cursor(1, 1);
    oled_write_ln_P(message, false);
}

void write_layer(const char *message) {
    oled_set_cursor(1, 2);
    oled_write_ln_P(message, false);
}

void os_scroll_line_message(void) {
    write_line(os_line);
}

void os_scroll_page_message(void) {
    write_line(os_page);
}

void iterm_scroll_line_message(void) {
    write_line(iterm_line);
}

void iterm_scroll_page_message(void) {
    write_line(iterm_page);
}

void vim_scroll_page_message(void) {
    write_line(vim_page);
}

void vim_scroll_line_message(void) {
    write_line(vim_line);
}

void vim_search_oled_setup(void) {
    write_line(vim_next_prev);
}

void vim_up_down_oled_setup(void) {
    write_line(vim_up_down);
}

void vim_jump_oled_setup(void) {
    write_line(vim_jump);
}

void main_layer_oled_setup(void) {
    write_layer(layer_main);
}

void terraria_layer_oled_setup(void) {
    write_layer(layer_terraria);
}

void starcraft_layer_oled_setup(void) {
    write_layer(layer_starcraft);
}

#endif

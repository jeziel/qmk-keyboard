#include QMK_KEYBOARD_H

typedef enum EventType_ {
    EVENT_NONE,
    MACRO_LAYER_CHANGE,
    VIM_SEARCH,
    VIM_SEARCH_CLEAR,
    VIM_TERM_SEARCH,
    VIM_TERM_SEARCH_CLEAR,
    VIM_MARK_SET,
    VIM_MARK_GO,
    VIM_FILES_OPEN,
    VIM_FILES_CLOSE,
    VIM_TREE,
    VIM_EASY_MOTION,
    VIM_GOTO_DEFINITION,
    ITM_SCROLL,
    ITM_FIND,
    MOZ_SEARCH,
    MOZ_SEARCH_CLEAR,
    MOZ_TERM_SEARCH,
    ROTARY_CLICK,
    ROTARY_CW,
    ROTARY_CCW,
    MAIN_SCROLL,
    EDIT,
    LAYER_CHANGE,
    LAYER_TOGGLE,

    // Rotary generated events. This family of events is for when the rotary
    // wants to notify the action that will happen when it clicks.
    // Depending on what I'm doing on the keyboard I can toggle what the
    // rotary will do. For example, when searching in VIM , the rotary by default
    // will go to the next/prev match, if I press the rotary button, then
    // the rotary will scroll by line, if I press again it goes back to
    // next/prev, and so on.
    // The reason I want to emit an event when the rotary toggles the action is
    // to let the oled know what to display (and whatever else needs to react
    // to the event and do something).
    ROTARY_UD,
    ROTARY_PGUD,
    ROTARY_VLSCR,
    ROTARY_VPSCR,
    ROTARY_VSRCH,
    ROTARY_ILSCR,
    ROTARY_IPSCR,
    ROTARY_VJUMP,
} EventType;

typedef enum {
    NO_ROTARY = -1,
    PRIMARY_ROTARY,
    SECONDARY_ROTARY,
} Rotary;

typedef struct {
    EventType event_type;
    uint8_t tap_count;
    Rotary rotary_position;
    uint8_t layer;
} Event;

typedef void (* RotaryEventHandler)(Event event);
typedef void (* KeyboardEventHandler)(Event event); // TODO. not needed?
typedef void (* ScTeamSequence)(uint16_t team_key);

typedef struct {
    uint16_t team_key;
    ScTeamSequence double_tap;
    ScTeamSequence triple_tap;
} ScTap;

typedef struct {
    RotaryEventHandler turn;
    RotaryEventHandler click;

    bool vim_search_rotary_toggle_state;
    bool vim_scroll_rotary_toggle_state;
    bool vim_jump_rotary_toggle_state;
    bool main_default_rotary_toggle_state;
    bool iterm_scroll_rotary_toggle_state;
} RotaryState;

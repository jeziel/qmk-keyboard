#ifdef ROTARY_ENABLED
#include QMK_KEYBOARD_H

void toggle_click_handler(
        bool *toggle,
        RotaryEventHandler *handler,
        RotaryEventHandler handler_state1,
        RotaryEventHandler handler_state2) {
    bool toggle_state = *toggle;
    *handler = (toggle_state) ? handler_state1 : handler_state2;
    *toggle = !toggle_state;
}

void emit_rotary_handler_event(Event event, bool toggle, EventType active, EventType inactive) {
#ifdef EVENTS_ENABLED
    emit_rotary_event(toggle ? active : inactive, event.rotary_position);
#endif
}

RotaryState *get_state(Event event);

void set_rotary_handlers(Event event, RotaryEventHandler turn, RotaryEventHandler click) {
    RotaryState *rotary_state = get_state(event);

    rotary_state->turn = turn;
    rotary_state->click = click;

    rotary_state->vim_search_rotary_toggle_state = false;
    rotary_state->vim_scroll_rotary_toggle_state = false;
    rotary_state->vim_jump_rotary_toggle_state = false;
    rotary_state->main_default_rotary_toggle_state = false;
    rotary_state->iterm_scroll_rotary_toggle_state = false;
}

// HANDLERS

void rotary_turn_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    rotary_state->turn(event);
}

void rotary_click_handler(Event event) {
    RotaryState *rotary_state = get_state(event);
    rotary_state->click(event);
}

#endif

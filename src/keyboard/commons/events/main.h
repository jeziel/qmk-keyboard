#ifdef EVENTS_ENABLED

void _emit_event_multiple(Event event);

void emit_tap_event(EventType event_type, uint8_t tap_count) {
    Event event = {event_type, tap_count, NO_ROTARY, -1};
    _emit_event_multiple(event);
}

void emit_key_event(EventType event_type) {
    Event event = {event_type, 0, NO_ROTARY, -1};
    _emit_event_multiple(event);
}

void emit_rotary_event(EventType event_type, Rotary rotary) {
    Event event = {event_type, 0, rotary, -1};
    _emit_event_multiple(event);
}

void emit_layer_event(uint8_t layer) {
    Event event = {LAYER_CHANGE, 0, NO_ROTARY, layer};
    _emit_event_multiple(event);
}

#endif

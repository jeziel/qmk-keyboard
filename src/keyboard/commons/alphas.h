#include QMK_KEYBOARD_H

// Main alphas
#define AL_R1 KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y
#define AL_R2 KC_A,    KC_O,    KC_E,    KC_U,    KC_I
#define AL_R3 LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X

#define AR_R1 KC_F,    KC_G,    KC_C,   KC_R, KC_L
#define AR_R2 KC_D,    KC_H,    KC_T,   KC_N, KC_S
#define AR_R3 KC_B,    KC_M,    KC_W,   KC_V, LS_Z

// Lower common.
#define LL_R1 DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB
#define LL_R2 KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP
#define LL_R3 XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX,   KC_PGDN
#define LL_R4 _______, _______,   _______

#define LR_R1 DN_VSCH, KC_7,    KC_8,    KC_9, DN_SCRL
#define LR_R2 DN_VTSH, KC_4,    KC_5,    KC_6, KC_0
#define LR_R3 DN_VMRK, KC_1,    KC_2,    KC_3, KC_KP_DOT
#define LR_R4 DN_VFLS, CT_VTRE, CT_VEZM

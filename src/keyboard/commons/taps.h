#include QMK_KEYBOARD_H

#ifdef TAP_DANCE_ENABLE

typedef struct {
    EventType event1;
    EventType event2;
} EventTypePair;

void double_tap_event(tap_dance_state_t *state, void *user_data) {
#ifdef EVENTS_ENABLED
    EventTypePair *pair = (EventTypePair *)user_data;
    emit_key_event((state->count == 2)? pair->event2 : pair->event1);
#endif
}

#define ACTION_DOUBLE_TAP_EVENT(event1, event2) \
    { .fn = {NULL, double_tap_event, NULL}, .user_data = (void *)&((EventTypePair){event1, event2}), }

void team_tap(tap_dance_state_t *state, void *user_data) {
    ScTap *team_data = (ScTap *) user_data;
    switch(state->count) {
        case 2: team_data->double_tap(team_data->team_key); break;
        case 3: team_set(team_data->team_key); break;
        default: team_select(team_data->team_key); break;
    }
}

#define TEAM_TAP(team_key, double_tap) \
    { .fn = {NULL, team_tap, NULL}, .user_data = (void *) &((ScTap) {team_key, double_tap, NULL}), }


void view_tap(tap_dance_state_t *state, void *user_data) {
    ScTap *team_data = (ScTap *) user_data;
    switch(state->count) {
        case 2: view_set(team_data->team_key); break;
        default: view_goto(team_data->team_key); break;
    }
}

#define VIEW_TAP(view_key) \
    { .fn = {NULL, view_tap, NULL}, .user_data = (void *) &((ScTap) {view_key, NULL, NULL}), }

#endif

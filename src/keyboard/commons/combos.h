#if defined(COMBO_ENABLE) && ! defined(CUSTOM_COMBOS)

#define R2_C1 KC_E

enum combos {
  CMB_ESC,
  CMB_CAPS,
  CMB_TAB,
  CMB_UNDS,
  CMB_TILDE,
  CMB_SLASH,
  CMB_COLON,
  CMB_BKSP,

  CMB_LPRN,
  CMB_RPRN,
  CMB_LBRC,
  CMB_RBRC,
  CMB_LCBR,
  CMB_RCBR,
  CMB_LT,
  CMB_GT,

  CMB_DQUO,
  CMB_EXCL,
  CMB_AT,
  CMB_HASH,
  CMB_DLR,
  CMB_PERC,
  CMB_DASH,
  CMB_EQ,
  CMB_QUES,

  CMB_ASTR,
  CMB_ENTR,
  CMB_GRVE,
  CMB_BKSLSH,
  CMB_PIPE,
  CMB_PLUS,

  CMB_AMP,
  CMB_CIRC,

  CMB_FK_ROT,

  CMB_NONE,
};

const uint16_t PROGMEM esc_combo[] = {KC_QUOT, KC_P, COMBO_END};
const uint16_t PROGMEM caps_combo[] = {KC_A, KC_U, COMBO_END};
const uint16_t PROGMEM entr_combo[] = {KC_S, KC_H, COMBO_END};
const uint16_t PROGMEM bksp_combo[] = {KC_C, KC_R, COMBO_END};

const uint16_t PROGMEM tilde_combo[] = {KC_DOT, KC_P, COMBO_END};
const uint16_t PROGMEM tab_combo[] = {R2_C1, KC_U, COMBO_END};
const uint16_t PROGMEM grve_combo[] = {KC_J, KC_K, COMBO_END};

const uint16_t PROGMEM slash_combo[] = {KC_N, KC_S, COMBO_END};
const uint16_t PROGMEM colon_combo[] = {KC_W, KC_V, COMBO_END};
const uint16_t PROGMEM dquo_combo[] = {KC_QUOT, KC_COMM, COMBO_END};

// row 1/row 2 combos
const uint16_t PROGMEM excl_combo[] = {KC_QUOT, KC_A, COMBO_END};
const uint16_t PROGMEM at_combo[] = {KC_COMM, KC_O, COMBO_END};
const uint16_t PROGMEM hash_combo[] = {KC_DOT, R2_C1, COMBO_END};
const uint16_t PROGMEM dlr_combo[] = {KC_P, KC_U, COMBO_END};
const uint16_t PROGMEM perc_combo[] = {KC_Y, KC_I, COMBO_END};
const uint16_t PROGMEM bksl_combo[] = {KC_F, KC_D, COMBO_END};
const uint16_t PROGMEM astr_combo[] = {KC_L, KC_S, COMBO_END};
const uint16_t PROGMEM unds_combo[] = {KC_G, KC_H, COMBO_END};
const uint16_t PROGMEM dash_combo[] = {KC_C, KC_T, COMBO_END};
const uint16_t PROGMEM eq_combo[] = {KC_R, KC_N, COMBO_END};

const uint16_t PROGMEM ques_combo[] = {KC_R, KC_L, COMBO_END};

const uint16_t PROGMEM lbrc_combo[] = {KC_G, KC_C, COMBO_END};
const uint16_t PROGMEM rbrc_combo[] = {KC_G, KC_R, COMBO_END};
const uint16_t PROGMEM lprn_combo[] = {KC_H, KC_T, COMBO_END};
const uint16_t PROGMEM rprn_combo[] = {KC_H, KC_N, COMBO_END};
const uint16_t PROGMEM lcbr_combo[] = {KC_M, KC_W, COMBO_END};
const uint16_t PROGMEM rcbr_combo[] = {KC_M, KC_V, COMBO_END};
const uint16_t PROGMEM lt_combo[] = {KC_H, KC_C, COMBO_END};
const uint16_t PROGMEM gt_combo[] = {KC_H, KC_R, COMBO_END};

const uint16_t PROGMEM pipe_combo[] = {KC_B, KC_D, COMBO_END};
const uint16_t PROGMEM plus_combo[] = {KC_H, KC_M, COMBO_END};
const uint16_t PROGMEM amp_combo[] = {KC_T, KC_W, COMBO_END};
const uint16_t PROGMEM circ_combo[] = {KC_N, KC_V, COMBO_END};

const uint16_t PROGMEM fake_rotary_click[] = {FK_RCCW, FK_RCW, COMBO_END};

combo_t key_combos[] = {
  [CMB_ESC]  = COMBO(esc_combo, KC_ESC),
  [CMB_CAPS] = COMBO(caps_combo, KC_CAPS),
  [CMB_TAB]  = COMBO(tab_combo, KC_TAB),
  [CMB_ENTR] = COMBO(entr_combo, KC_ENT),
  [CMB_BKSP] = COMBO(bksp_combo, KC_BSPC),
  [CMB_EXCL] = COMBO(excl_combo, KC_EXLM),
  [CMB_AT]   = COMBO(at_combo, KC_AT),
  [CMB_HASH] = COMBO(hash_combo, KC_HASH),
  [CMB_DLR]  = COMBO(dlr_combo, KC_DLR),
  [CMB_PERC] = COMBO(perc_combo, KC_PERC),

  [CMB_DQUO] = COMBO(dquo_combo, KC_DQUO),
  [CMB_GRVE] = COMBO(grve_combo, KC_GRAVE),
  [CMB_QUES] = COMBO(ques_combo, KC_QUES),
  [CMB_PLUS] = COMBO(plus_combo, KC_PLUS),
  [CMB_UNDS] = COMBO(unds_combo, KC_UNDS),
  [CMB_DASH] = COMBO(dash_combo, KC_MINS),
  [CMB_EQ]   = COMBO(eq_combo, KC_EQL),
  [CMB_ASTR] = COMBO(astr_combo, KC_ASTR),
  [CMB_AMP]  = COMBO(amp_combo, KC_AMPR),
  [CMB_CIRC] = COMBO(circ_combo, KC_CIRC),

  [CMB_TILDE]  = COMBO(tilde_combo, KC_TILD),
  [CMB_SLASH]  = COMBO(slash_combo, KC_KP_SLASH),
  [CMB_BKSLSH] = COMBO(bksl_combo, KC_BACKSLASH),
  [CMB_PIPE]   = COMBO(pipe_combo, KC_PIPE),
  [CMB_COLON]  = COMBO(colon_combo, KC_COLN),

  [CMB_LPRN] = COMBO(lprn_combo, KC_LPRN),
  [CMB_RPRN] = COMBO(rprn_combo, KC_RPRN),
  [CMB_LBRC] = COMBO(lbrc_combo, KC_LBRC),
  [CMB_RBRC] = COMBO(rbrc_combo, KC_RBRC),
  [CMB_LCBR] = COMBO(lcbr_combo, KC_LCBR),
  [CMB_RCBR] = COMBO(rcbr_combo, KC_RCBR),
  [CMB_LT]   = COMBO(lt_combo, KC_LT),
  [CMB_GT]   = COMBO(gt_combo, KC_GT),

  [CMB_FK_ROT] = COMBO(fake_rotary_click, FK_RCLK),
};

/*
uint16_t prev_timer = 0;
uint8_t prev_combo = CMB_NONE;

void reset_combo_data(void) {
    prev_timer = timer_read();
    prev_combo = CMB_NONE;
}

bool is_in_tap_period(void) {
    uint16_t elapsed = timer_elapsed(prev_timer);

    bool in_period = (elapsed < TAPPING_TERM);
    prev_timer = timer_read();

    return in_period;
}

bool is_same_combo(uint8_t current_combo) {
    bool same = prev_combo == current_combo;
    prev_combo = current_combo;

    return same;
}

bool is_tapping_combos(uint8_t combo_index) {
    bool in_period = is_in_tap_period();
    bool same = is_same_combo(combo_index);
    bool is_tapping = (same && in_period);

    return is_tapping;
}

void select_key(uint16_t regular, uint16_t tapping, uint8_t combo_index) {
    bool is_tapping = is_tapping_combos(combo_index);
    tap_code16((is_tapping) ? tapping : regular);
    // when the tap has been written, we are done, and reset.
    if (is_tapping) { reset_combo_data(); }
}

void process_combo_event(uint8_t combo_index, bool pressed) {
    // TODO. fix the case when we left a combo pressed
    if (!pressed) { return; }

    switch(combo_index) {
        case CMB_LPRN:
            select_key(KC_LPRN, KC_RPRN, combo_index);
            break;
        case CMB_LCBR:
            select_key(KC_LCBR, KC_RCBR, combo_index);
            break;
        case CMB_LBRC:
            select_key(KC_LBRC, KC_RBRC, combo_index);
            break;
        default:
            reset_combo_data();
    }
}
*/

#endif

#pragma once

#define COMBO_TERM 40

#if ! defined(COMBO_COUNT)
  #define COMBO_COUNT 34
#endif

#ifndef TAPPING_TERM
  #define TAPPING_TERM 175
#endif

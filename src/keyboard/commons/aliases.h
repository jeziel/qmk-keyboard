
#define LOWER MO(_LOWER)

#define LWR_BKS   LT(_LOWER, KC_BSPC) /* lower when hold, backspace when tapped */
#define LWR_E     LT(_LOWER, KC_E)    /* lower when hold, e when tapped */
#define LS_SCLN   LSFT_T(KC_SCLN)     /* left shift when hold, semicolon when tapped */
#define LS_Z      LSFT_T(KC_Z)        /* left shift when hold, z when tapped */
#define LS_TAB    LSFT_T(KC_TAB)      /* left shift+Tab */
#define CM_ENT    LGUI_T(KC_ENT)      /* left cmd when hold, enter when tapped */
#define SCN_SHT   SGUI(KC_4)          /* Shift-Cmd-4: Screenshot */
#define LS_CAPS   LSFT_T(KC_CAPS)     /* Left Shift when hold, CAPS when tapped*/
#define LC_DOWN   LCTL_T(KC_DOWN)

// VIM
#define VIM_NEXT_FIND   KC_N
#define VIM_PREV_FIND   LSFT(KC_N)
#define VIM_NEXT_WORD   LSFT(KC_W)
#define VIM_PREV_WORD   LSFT(KC_B)
#define VIM_LINE_UP     LCTL(KC_Y)
#define VIM_LINE_DOWN   LCTL(KC_E)
#define VIM_BLOCK_UP    LCTL(KC_U)
#define VIM_BLOCK_DOWN  LCTL(KC_D)
#define VIM_WINDOW      LCTL(KC_W)

// MOZ
#define TAB_RIGHT    LCTL(KC_TAB)
#define TAB_LEFT     LCTL(LSFT(KC_TAB))
#define FIRST_TAB    LGUI(KC_1)
#define LAST_TAB     LGUI(KC_9)
#define CLOSE_TAB    LGUI(KC_W)
#define BACK_PAGE    LGUI(KC_LEFT)
#define FORWARD_PAGE LGUI(KC_RIGHT)

// GNRL
#define DSKTP_NAV_LEFT  LCTL(KC_LEFT)
#define DSKTP_NAV_RIGHT LCTL(KC_RIGHT)
#define DTP_LEFT        DSKTP_NAV_LEFT
#define DTP_RIGHT       DSKTP_NAV_RIGHT
#define LINE_START      LGUI(KC_LEFT)
#define LINE_END        LGUI(KC_RIGHT)
#define WORD_BACK       LALT(KC_LEFT)
#define WORD_FORWARD    LALT(KC_RIGHT)
#define GRL_FIND        KC_F3
#define GRL_PREV_FIND   LSFT(KC_F3)
#define GRL_COPY        LGUI(KC_C)
#define GRL_CUT         LGUI(KC_X)

// ITERM
#define ITERM_LINE_UP   LGUI(KC_UP)
#define ITERM_LINE_DOWN LGUI(KC_DOWN)
#define ITERM_PGUP      LGUI(KC_PGUP)
#define ITERM_PGDOWN    LGUI(KC_PGDN)
#define ITERM_CLEAR_BUFFER LGUI(KC_K)
#define ITERM_FIND_NEXT KC_ENT
#define ITERM_FIND_PREV LSFT(KC_ENT)

// SC
#define SC_MOVE KC_COMM
#define SC_ATCK KC_P
#define SC_PTRL KC_O
#define SC_RLY  KC_F

#define SC_VIEW_1 KC_F2
#define SC_VIEW_2 KC_F3
#define SC_VIEW_3 KC_F4

#define GT_SC   TG(_SC)
#define GT_TRR  TG(_TRR)
#define GT_LMB  TG(_LMB)
#define MO_TEAM MO(_SC_T)
#define MO_TRR  MO(_T_WP)

// Fake rotary
#define FK_RCCW KC_F23
#define FK_RCW  KC_F24
#define FK_RCLK KC_F22

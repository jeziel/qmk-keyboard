
#include QMK_KEYBOARD_H


void up(void) {
    tap_code16(KC_UP);
}

void down(void) {
    tap_code16(KC_DOWN);
}

void enter(void) {
    tap_code(KC_ENT);
}

void soft_scroll_down(void) {
    down();
    down();
    down();
}

void soft_scroll_up(void) {
    up();
    up();
    up();
}

void desktop_left(void) {
    tap_code16(DTP_LEFT);
}

void desktop_right(void) {
    tap_code16(DTP_RIGHT);
}

void pgup(void) {
    tap_code16(KC_PGUP);
}

void pgdown(void) {
    tap_code16(KC_PGDN);
}

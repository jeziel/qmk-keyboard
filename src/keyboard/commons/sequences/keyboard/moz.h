
#include QMK_KEYBOARD_H

void moz_term_search(void) {
    tap_code16(LGUI(KC_C));
    tap_code16(LGUI(KC_T));
    tap_code16(LGUI(KC_V));
    tap_code(KC_ENTER);
}

void moz_search(void) {
    tap_code16(LGUI(KC_C));
    tap_code16(LGUI(KC_F));
    tap_code16(LGUI(KC_V));
}

void moz_search_clear(void) {
    tap_code16(LGUI(KC_F));
    tap_code16(LGUI(KC_A));
    tap_code(KC_BSPC);
    tap_code(KC_ENT);
    tap_code(KC_ESC);
}

void moz_close_tab(void) {
    tap_code16(CLOSE_TAB);
}

void moz_tab_right(void) {
    tap_code16(TAB_RIGHT);
}

void moz_tab_left(void) {
    tap_code16(TAB_LEFT);
}

void moz_next_search(void) {
    tap_code16(GRL_FIND);
}

void moz_previous_search(void) {
    tap_code16(GRL_PREV_FIND);
}

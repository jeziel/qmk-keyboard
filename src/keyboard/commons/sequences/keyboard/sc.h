#include QMK_KEYBOARD_H

void team_action(uint16_t team_key, uint16_t team_action) {
    tap_code16(team_key);
    tap_code16(team_action);
    wait_ms(50);
    tap_code(KC_BTN1);
}

void team_attack(uint16_t team_key) {
    team_action(team_key, SC_ATCK);
}

void team_move(uint16_t team_key) {
    team_action(team_key, SC_MOVE);
}

void team_patrol(uint16_t team_key) {
    team_action(team_key, SC_PTRL);
}

void team_set(uint16_t team_key) {
    register_code(KC_LGUI);
    tap_code16(team_key);
    unregister_code(KC_LGUI);
}

void team_select(uint16_t team_key) {
    tap_code16(team_key); 
}

void view_set(uint16_t view_key) {
    register_code(KC_LSFT);
    tap_code16(view_key);
    unregister_code(KC_LSFT);
}

void view_goto(uint16_t view_key) {
    tap_code16(view_key);
}


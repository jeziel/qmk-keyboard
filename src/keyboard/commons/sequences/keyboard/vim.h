
#include QMK_KEYBOARD_H

void vim_tree(void) {
    // this requires configuration in vimrc
    // nnoremap <leader>t :NERDTreeToggle<cr>
    tap_code(KC_ESC);
    tap_code(KC_BSLS);
    tap_code(KC_T);
}

void vim_term_search(void) {
    tap_code(KC_ESC);
    SEND_STRING("\"*yaw:Rg " SS_LGUI("v"));
    tap_code(KC_ENT);
}

void vim_easymotion(void) {
    // easy motion "ESC \\s"
    tap_code(KC_ESC);
    tap_code(KC_BSLS);
    tap_code(KC_BSLS);
    tap_code(KC_S);
}

void vim_fzf(void) {
    // .vimrc conf: nnoremap <leader><space> :Files<cr>¬
    tap_code(KC_ESC);
    tap_code(KC_BSLS);
    tap_code(KC_SPC);
}

void vim_find_clear(void) {
    //<ESC>:noh<ENT>
    tap_code(KC_ESC);
    tap_code16(KC_COLN);
    tap_code(KC_N);
    tap_code(KC_O);
    tap_code(KC_H);
    tap_code(KC_ENT);
}

void vim_find(void) {
    tap_code(KC_ESC);
    tap_code16(KC_ASTR);
}

void vim_mark_set(void) {
    tap_code(KC_M);
    tap_code(KC_A);
}

void vim_mark_go(void) {
    tap_code(KC_QUOT);
    tap_code(KC_A);
}

void vim_write(void) {
    tap_code(KC_ESC);
    tap_code16(KC_COLN);
    tap_code(KC_W);
    tap_code(KC_ENT);
}

void vim_escape(void) {
    tap_code(KC_ESC);
}

void vim_next_search(void) {
    tap_code(VIM_NEXT_FIND);
}

void vim_previous_search(void) {
    tap_code16(VIM_PREV_FIND);
}

void vim_scroll_line_up(void) {
    tap_code16(VIM_LINE_UP);
}

void vim_scroll_line_down(void) {
    tap_code16(VIM_LINE_DOWN);
}

void vim_scroll_block_up(void) {
    tap_code16(VIM_BLOCK_UP);
}

void vim_scroll_block_down(void) {
    tap_code16(VIM_BLOCK_DOWN);
}

void vim_ale_goto_definition(void) {
    // .vimrc conf: nnoremap <leader>g :ALEGoToDefinition<cr>
    tap_code16(KC_ESC);
    tap_code16(KC_BSLS);
    tap_code16(KC_G);
}

void vim_jump_forward(void) {
    register_code(KC_LCTL);
    tap_code16(KC_I);
    unregister_code(KC_LCTL);
}

void vim_jump_back(void) {
    register_code(KC_LCTL);
    tap_code16(KC_O);
    unregister_code(KC_LCTL);
}


#include QMK_KEYBOARD_H

void iterm_page_up(void) {
    tap_code16(ITERM_PGUP);
}

void iterm_page_down(void) {
    tap_code16(ITERM_PGDOWN);
}

void iterm_line_up(void) {
    tap_code16(ITERM_LINE_UP);
}

void iterm_line_down(void) {
    tap_code16(ITERM_LINE_DOWN);
}

void iterm_find_up(void) {
    tap_code16(ITERM_FIND_NEXT);
}

void iterm_find_down(void) {
    tap_code16(ITERM_FIND_PREV);
}


#include QMK_KEYBOARD_H

//
// TURNS
//

void moz_tab_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: moz_tab_right(); break;
        case ROTARY_CCW: moz_tab_left(); break;
        default: break;
    }
}

void moz_soft_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: soft_scroll_down(); break;
        case ROTARY_CCW: soft_scroll_up(); break;
        default: break;
    }
}

void moz_search_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: moz_next_search(); break;
        case ROTARY_CCW: moz_previous_search(); break;
        default: break;
    }
}


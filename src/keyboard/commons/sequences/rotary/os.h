
#include QMK_KEYBOARD_H

//
// TURNS
//

void main_desktop_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: desktop_right(); break;
        case ROTARY_CCW: desktop_left(); break;
        default: break;
    }
}

void main_up_down_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: down(); break;
        case ROTARY_CCW: up(); break;
        default: break;
    }
}

void main_pgup_down_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: pgdown(); break;
        case ROTARY_CCW: pgup(); break;
        default: break;
    }
}


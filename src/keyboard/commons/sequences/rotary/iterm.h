#include QMK_KEYBOARD_H

//
// TURNS
//

void iterm_line_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: iterm_line_down(); break;
        case ROTARY_CCW: iterm_line_up(); break;
        default: break;
    }
}

void iterm_block_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: iterm_page_down(); break;
        case ROTARY_CCW: iterm_page_up(); break;
        default: break;
    }
}

void iterm_find_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: iterm_find_down(); break;
        case ROTARY_CCW: iterm_find_up(); break;
        default: break;
    }
}


#include QMK_KEYBOARD_H

//
// TURNS
//

void vim_line_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: vim_scroll_line_down(); break;
        case ROTARY_CCW: vim_scroll_line_up(); break;
        default: break;
    }
}

void vim_block_scroll_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: vim_scroll_block_down(); break;
        case ROTARY_CCW: vim_scroll_block_up(); break;
        default: break;
    }
}

void vim_search_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: vim_next_search(); break;
        case ROTARY_CCW: vim_previous_search(); break;
        default: break;
    }
}

void vim_jump_rotary_turn_handler(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case ROTARY_CW: vim_jump_forward(); break;
        case ROTARY_CCW: vim_jump_back(); break;
        default: break;
    }
}


//
// CLICKS
//

void vim_files_rotary_click_handler(Event event) {
    enter();
#ifdef EVENTS_ENABLED
    emit_key_event(VIM_FILES_CLOSE);
#endif
}

void vim_term_search_rotary_click_handler(Event event) {
    enter();
#ifdef EVENTS_ENABLED
    emit_key_event(VIM_TERM_SEARCH_CLEAR);
#endif
}

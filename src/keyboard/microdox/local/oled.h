#ifdef OLED_ENABLE

#include QMK_KEYBOARD_H

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    oled_scroll_set_area(1, 2);
    return OLED_ROTATION_0;
}

bool oled_task_user(void) {
    return false;
}

#endif

/* Copyright 2020 Boardsource
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

#include "jeziel.h"

typedef enum Layers {
    _MAIN,
    _TRR,
    _SC,
    _LOWER,
    _SC_T,
    _T_WP,
} Layers;

#include "local/main.h"

enum custom_keycodes {
    CT_VTRE = SAFE_RANGE,
	CT_VEZM,
    CT_VGTD,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_MAIN] = LAYOUT_ortho_4x12(
        KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,    XXXXXXX, XXXXXXX, KC_F,   KC_G,    KC_C,    KC_R,    KC_L,
        KC_A,    KC_O,    KC_E,    KC_U,    KC_I,    XXXXXXX, XXXXXXX, KC_D,   KC_H,    KC_T,    KC_N,    KC_S,
        LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    XXXXXXX, XXXXXXX, KC_B,   KC_M,    KC_W,    KC_V,    LS_Z,
        XXXXXXX, XXXXXXX, KC_LALT, KC_LCMD, LOWER,   XXXXXXX, XXXXXXX, KC_SPC, KC_RCTL, XXXXXXX, FK_RCCW, FK_RCW
    ),

    [_TRR] = LAYOUT_ortho_4x12(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______,
        _______, _______, KC_LCTL, MO_TRR,  KC_SPC,  _______, _______, _______, _______, GT_TRR,  _______,  _______
    ),

    [_SC] = LAYOUT_ortho_4x12(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, KC_LCTL, MO_TEAM, KC_SPC,  _______, _______, _______, _______, GT_SC,   _______,  _______
    ),
        

    [_LOWER] = LAYOUT_ortho_4x12(
        DN_HMND, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB, _______, _______, DN_VSCH, KC_7,    KC_8,    KC_9,    CT_VGTD,
        KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,   _______, _______, DN_VTSH, KC_4,    KC_5,    KC_6,    KC_0,
        GT_SC,   GT_TRR,   XXXXXXX, XXXXXXX,   KC_PGDN,   _______, _______, DN_VMRK, KC_1,    KC_2,    KC_3,    XXXXXXX,
        _______, _______,  _______, _______,   _______,   _______, _______, DN_VFLS, CT_VTRE, CT_VEZM, _______, GT_SC
    ),

    [_SC_T] = LAYOUT_ortho_4x12(
        _______,  DN_TM1,  DN_TM2,  DN_TM3,  _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  DN_TM4,  DN_TM5,  DN_TM6,  _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  DN_TM7,  DN_TM8,  DN_TM9,  _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

    [_T_WP] =  LAYOUT_ortho_4x12(
        KC_1,    KC_3,    KC_5,    KC_7,    KC_9,    _______, _______, _______, _______, _______, _______, _______,
        KC_2,    KC_4,    KC_6,    KC_8,    KC_0,    _______, _______, _______, _______, _______, _______, _______,
        KC_F1,   KC_F2,   KC_F3,   _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, KC_LCMD, MO_TRR,  KC_SPC,  _______, _______, _______, _______, _______, _______, _______
    ),
        
    };

void keyboard_post_init_user(void) {
#ifdef EVENTS_ENABLED
    emit_key_event(MAIN_SCROLL);
#endif
}

bool process_record_user_pressed(uint16_t keycode) {
    switch(keycode) {
#ifdef EVENTS_ENABLED
        case FK_RCCW: emit_rotary_event(ROTARY_CCW, PRIMARY_ROTARY); break;
        case FK_RCW:  emit_rotary_event(ROTARY_CW, PRIMARY_ROTARY); break;
        case FK_RCLK: emit_rotary_event(ROTARY_CLICK, PRIMARY_ROTARY); break;
        case CT_VEZM: emit_key_event(VIM_EASY_MOTION); break;
        case CT_VTRE: emit_key_event(VIM_TREE); break;
        case CT_VGTD: emit_key_event(VIM_GOTO_DEFINITION); break;
#endif
        default: break;
    }

    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef EVENTS_ENABLED
    if (record->event.pressed) {
        return process_record_user_pressed(keycode);
    }
#endif

    return true;
};


#ifdef ROTARY_ENABLED

RotaryState secondary_rotary_state;
RotaryState *get_state(Event event) {
    return &secondary_rotary_state;
}

#endif

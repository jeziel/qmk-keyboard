#ifdef RGB_MATRIX_ENABLE

#include QMK_KEYBOARD_H

enum _rgb{_R, _G, _B};

#define BLACK { RGB_BLACK }
#define GREEN { RGB_GREEN }
#define RED { RGB_RED }
#define PINK { RGB_PINK }
#define BLUE { RGB_BLUE }
#define GOLD { RGB_GOLD }
#define CYAN { RGB_CYAN }
#define WHITE { RGB_WHITE }

#define BACK 0
#define R0 10
#define R1 22
#define R2 34
#define R3 46
#define TOTAL_LEDS 48

const uint8_t PROGMEM sc_colors[TOTAL_LEDS][3] = {
    RED,   PINK,  RED,   GREEN, GREEN, GREEN, BLUE,  GOLD, BLACK, BLACK, BLACK, BLACK,
    RED,   PINK,  RED,   GREEN, GREEN, GREEN, CYAN,  GOLD, BLACK, BLACK, BLACK, BLACK,
    RED,   RED,   RED,   GREEN, GREEN, GREEN, BLUE,  GOLD, BLACK, BLACK, BLACK, BLACK,
    GREEN, GREEN, GREEN, BLUE,  BLUE,  BLUE,  WHITE, GOLD, BLACK, BLACK, BLACK, BLACK,
    };


void paint_line(RGB color, uint8_t start, uint8_t length) {
    uint8_t end = start + length;
    for(int i = start; i < end; i ++) {
        rgb_matrix_set_color(i, color.r, color.g, color.b);
    }
}

void main_layer_rgb(void) {
    rgb_matrix_set_color(R1 + 3, RGB_RED);
    rgb_matrix_set_color(R1 + 8, RGB_RED);
}

void sc_layer_rgb(void) {
  //float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
    for(int i = 0; i < TOTAL_LEDS; i+=1) {
        /*
        float r = sc_colors[i][_R] * f;
        float g = sc_colors[i][_G] * f;
        float b = sc_colors[i][_B] * f;
        */
        //rgb_matrix_set_color(R0 + i, RGB_RED);
        //rgb_matrix_set_color(R0 + i, 0xFF, 0xFF, 0x00);
        uint8_t r = sc_colors[i][_R];
        uint8_t g = sc_colors[i][_G];
        uint8_t b = sc_colors[i][_B];
        rgb_matrix_set_color(R0 + i, r, g, b);
    }

    // paint_line(green, R2 + 3, 3);
    // rgb_matrix_set_color(R1 + 3, RED);
    // rgb_matrix_set_color(R1 + 8, RED);
}

typedef void (* RgbColorFun)(void);
RgbColorFun color_fun = main_layer_rgb;

void set_main_layer_rgb(void) {
    color_fun = main_layer_rgb;
}

void set_sc_layer_rgb(void) {
    color_fun = sc_layer_rgb;
}

void rgb_matrix_indicators_user(void) {
    color_fun();
}

#endif

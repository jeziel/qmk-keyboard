/* Good on you for modifying your layout! if you don't have
 * time to read the QMK docs, a list of keycodes can be found at
 *
 * https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
 *
 * There's also a template for adding new layers at the bottom of this file!
 */

#include QMK_KEYBOARD_H
#include "jeziel.h"

enum layers {
    _DVORAK=0,
    _LOWER,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_DVORAK] = LAYOUT_gergoplex(
    KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,   /* | */   KC_F,    KC_G,    KC_C,    KC_R,    KC_L,
    KC_A,    KC_O,    KC_E,    KC_U,    KC_I,   /* | */   KC_D,    KC_H,    KC_T,    KC_N,    KC_S,
    LS_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,   /* | */   KC_B,    KC_M,    KC_W,    KC_V,    LS_Z,
                      KC_LALT, KC_LCMD, LOWER,  /* | */   KC_SPC,  KC_RCTL, KC_CAPS
),

[_LOWER] = LAYOUT_gergoplex(
    XXXXXXX, TAB_LEFT, KC_UP,   TAB_RIGHT, CLOSE_TAB, /* | */ XXXXXXX, KC_7,    KC_8,   KC_9, XXXXXXX,
    KC_LSFT, KC_LEFT,  LC_DOWN, KC_RIGHT,  KC_PGUP,   /* | */ KC_HOME, KC_4,    KC_5,   KC_6, KC_0,
    XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX,   KC_PGDOWN, /* | */ KC_END,  KC_1,    KC_2,   KC_3, KC_KP_DOT,
                       _______, _______,   XXXXXXX,   /* | */ XXXXXXX, XXXXXXX, SCN_SHT
), 

};

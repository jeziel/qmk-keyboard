#ifdef EVENTS_ENABLED

void keyboard_event_listener(Event event) {
    EventType event_type = event.event_type;
    switch(event_type) {
        case VIM_SEARCH: vim_find(); break;
        case VIM_SEARCH_CLEAR: vim_find_clear(); break;
        case VIM_TERM_SEARCH: vim_term_search(); break;
        case VIM_TERM_SEARCH_CLEAR: vim_escape(); break;
        case VIM_EASY_MOTION: vim_easymotion(); break;
        case VIM_MARK_SET: vim_mark_set(); break;
        case VIM_MARK_GO: vim_mark_go(); break;
        case VIM_FILES_OPEN: vim_fzf(); break;
        case VIM_FILES_CLOSE: vim_escape(); break;
        case VIM_TREE: vim_tree(); break;
        case VIM_GOTO_DEFINITION: vim_ale_goto_definition(); break;
        default: break;
    }
}

void _emit_event_multiple(Event event) {
    keyboard_event_listener(event);
}

#endif

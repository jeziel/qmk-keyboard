
#--build-arg ARG_USER=$GITLAB_USER_NAME
# FROM registry.gitlab.com/jeziel/qmk-keyboard:master
FROM qmk-jeziel-base:latest

ARG ARG_QMK_HOME
ARG ARG_USER

ENV QMK_HOME=$ARG_QMK_HOME
ENV QMK_KEYBOARDS=$ARG_QMK_HOME/keyboards
ENV USER=$ARG_USER

RUN mkdir ${QMK_HOME}/users/${USER} && \
    qmk new-keymap -kb boardsource/microdox/v1 -km $USER && \
    qmk new-keymap -kb boardsource/technik_o -km $USER && \
    qmk new-keymap -kb keebio/bdn9 -km $USER && \
    qmk new-keymap -kb keebio/bfo9000 -km $USER && \
    qmk new-keymap -kb kingly_keys/romac_plus -km $USER && \
    qmk new-keymap -kb planck/ez -km $USER && \
    qmk new-keymap -kb kyria -km $USER && \
    qmk new-keymap -kb salicylic_acid3/naked48/rev1 -km $USER && \
    qmk new-keymap -kb marksard/rhymestone -km $USER && \
    qmk new-keymap -kb spaceman/pancake/rev2 -km $USER && \
    qmk new-keymap -kb ferris/sweep -km $USER && \
    qmk new-keymap -kb crkbd -km $USER

VOLUME ${QMK_KEYBOARDS}/boardsource/microdox/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/boardsource/technik_o/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/keebio/bdn9/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/keebio/bfo9000/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/kingly_keys/romac_plus/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/marksard/rhymestone/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/splitkb/kyria/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/planck/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/salicylic_acid3/naked48/rev1/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/spaceman/pancake/rev2/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/ferris/sweep/keymaps/${USER}
VOLUME ${QMK_KEYBOARDS}/crkbd/keymaps/${USER}
VOLUME ${QMK_HOME}/users/${USER}

# Set up.

We can pull the firmware to the image by running:

```
./operation.sh build
```

The script takes my username and uses it as the keymap name. And creates an
image using that value. As you can see in the Dockerfile I have everything
setup to configure my 2 keyboards, adding a new keyboard will require me to set
that up, in the `script/setup.sh` and a new volume to edit the code. Once the
code has been edited and I'm ready to test I compile:

```
./operation.sh compile gergoplex
```

# Flashing
It seems that Docker cannot easily access the USB devices connected to the
host. I'm going to try the [qmx toolkit][1].

```
brew tap homebrew/cask-drivers
brew cask install qmk-toolbox
```

# Adding a new keyboard

0. find the keyboard directory in `qmk_firmware`
1. add the keyboard to the `setup.sh` script. The keyboard normally says how
   you need to compile the firmware.
   - to get the binary name, I compile the keyboard in the container.
   - put the name as the 3rd param of the `compile` command.
2. add the volume to the Dockerfile
3. mount the volume in the compile option of `operation.sh`

Rebuild the image, create the firmware and flash the keyboard.

```
./operation.sh build
./operation.sh compile <new keyboard>
flaash with qmk toolbox
```

# My keyboards.

Before this list of keyboards, is hard for me to remember what keyboards
I used, probably one of those mac usb with the numpad. Oh well...

Matias was my entry into mechanical keyboards, I didn't know at that time what
mechanical was, I never wondered what was underneath the keycaps.

The Ergo Pro was my first split keyboard, I guess I was concerned about
ergonomics? Splits are a different thing when you don't touch type, I took it
out of the box started to type and realized that I wasn't gonna go too far. 
You can put one half next to the other but still, why buy a split to put it
together. At that time I only knew about Qwerty and Dvorak, so I went with
Dvorak and learn to touch type.

0. Several [Matias][m] keyboards
   - Matias wireless aluminum
   - Matias Laptop pro
   - Mini tactile pro
   - Ergo Pro
1. Planck EZ (from [ZSA][zsa]
2. GergoPlex (from [g Heavy Industries][ghi])
3. Kyria (from [LittleKeyboards][lk])
4. BDN9 (from [Keebio][kio])
5. RoMac+ (from [LittleKeyboards][lk])
6. BFO-9000 (from [Keebio][kio])
7. Naked48 (from [LittleKeyboards][lk])
8. Microdox (from [Boardsource][bs])
9. DracuLad (from [LittleKeyboards][lk])
10. Technik (from [Boardsource][bs])

# Touch typing

As I mentioned, I learned to touch type using Dvorak. At that time I only knew
of Antonin Dvorak, I thought it was cool that the creator of the layout aand
the composer had the same last name.

Learning to touch type is hard, frustrating and painful, painful out of the
frustration, and nothing about my fingers. Now that I've used Dvorak for some
time I realized how loaded my right hand is when typing.

[1]: https://qmk.fm/toolbox/
[m]: https://www.matias.ca "Matias"
[lk]: https://www.littlekeyboards.com/ "LittleKeyboards"
[ghi]: https://www.gboards.ca/ "g Heavy Industries"
[zsa]: https://ergodox-ez.com/ "ZSA"
[kio]: https://keeb.io/ "Keebio"
[bs]: https://boardsource.xyz/ "Boardsource"
